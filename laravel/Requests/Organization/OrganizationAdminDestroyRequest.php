<?php

namespace App\Http\Requests\Organization;

use App\Services\Organization\OrganizationService;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationAdminDestroyRequest extends FormRequest
{
    public function authorize(OrganizationService $service): bool
    {
        $org = $service->findOrFail($this->route('id'));

        return $this->user()->can('detach-admin', [$org, $this->route('userId')]);
    }

    public function rules(): array
    {
        return [];
    }
}
