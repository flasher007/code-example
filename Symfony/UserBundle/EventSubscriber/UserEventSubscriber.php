<?php

declare(strict_types=1);

/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace HypeCorp\Bundle\UserBundle\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use HypeCorp\Bundle\UserBundle\Entity\User;
use HypeCorp\Bundle\UserBundle\Util\TokenGenerator;
use Max107\SwiftMailerHelper\Mail;
use Scheb\TwoFactorBundle\Security\TwoFactor\Event\TwoFactorAuthenticationEvent;
use Scheb\TwoFactorBundle\Security\TwoFactor\Event\TwoFactorAuthenticationEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class UserListener
 */
class UserEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var Mail
     */
    protected $mail;

    /**
     * @var UrlGeneratorInterface
     */
    protected $generator;

    /**
     * @var TokenGenerator
     */
    protected $tokenGenerator;
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    /**
     * @var UserPasswordEncoderInterface
     */
    protected $passwordEncoder;
    /**
     * @var TranslatorInterface
     */
    protected $translator;
    /**
     * @var bool|int
     */
    protected $loginEmail;

    /**
     * UserListener constructor.
     *
     * @param Mail                         $mail
     * @param UrlGeneratorInterface        $generator
     * @param TokenGenerator               $tokenGenerator
     * @param TranslatorInterface          $translator
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface       $em
     * @param bool                         $loginEmail
     */
    public function __construct(
        Mail $mail,
        UrlGeneratorInterface $generator,
        TokenGenerator $tokenGenerator,
        TranslatorInterface $translator,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $em,
        $loginEmail = false
    ) {
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $em;
        $this->tokenGenerator = $tokenGenerator;
        $this->translator = $translator;
        $this->mail = $mail;
        $this->generator = $generator;
        $this->loginEmail = $loginEmail;
    }

    /**
     * @param UserEvent $event
     *
     * @throws \Exception
     */
    public function onUserLostPassword(UserEvent $event)
    {
        $user = $event->getUser();

        $this->mail->send(
            $this->translator->trans('email_reset_subject'),
            $user->getEmail(),
            '@User/mail/user/lost_password',
            [
                'subject' => $this->translator->trans('email_reset_subject'),
                'model' => $user,
                'confirm_url' => $this->generator->generate('user_lost_password_confirm', [
                    'token' => $user->getToken(),
                ], UrlGeneratorInterface::ABSOLUTE_URL),
            ]
        );
    }

    /**
     * @param UserEvent $event
     *
     * @throws \Exception
     */
    public function onUserRegistered(UserEvent $event)
    {
        $user = $event->getUser();

        $this->mail->send(
            $this->translator->trans('email_signup_subject'),
            $user->getEmail(),
            '@User/mail/user/registration',
            [
                'subject' => $this->translator->trans('email_signup_subject'),
                'model' => $user,
                'confirm_url' => $this->generator->generate('user_registration_confirm', [
                    'token' => $user->getToken(),
                ], UrlGeneratorInterface::ABSOLUTE_URL),
            ]
        );
    }

    /**
     * TODO remove me
     *
     * @param UserEvent $event
     *
     * @throws \Exception
     */
    public function onUserSocialRegistered(UserEvent $event)
    {
        $user = $event->getUser();

        $this->mail->send(
            $this->translator->trans('email_signup_subject'),
            $user->getEmail(),
            '@User/mail/user/registration',
            [
                'subject' => $this->translator->trans('email_signup_subject'),
                'model' => $user,
                'confirm_url' => $this->generator->generate('social_registration_confirm', [
                    'token' => $user->getToken(),
                ], UrlGeneratorInterface::ABSOLUTE_URL),
            ]
        );
    }

    /**
     * @param InteractiveLoginEvent $event
     *
     * @throws \Exception
     */
    public function onLogin(InteractiveLoginEvent $event)
    {
        if (!$this->loginEmail) {
            return;
        }

        /** @var User $user */
        $user = $event->getAuthenticationToken()->getUser();
        if ($user->getIsTwofactor()) {
            // Не присылаем пользователю уведомление, если включена 2fa авторизация
            return;
        }

        $this->mail->send(
            $this->translator->trans('user.mail.login_subject'),
            $user->getEmail(),
            '@User/mail/user/login',
            [
                'subject' => $this->translator->trans('user.mail.login_subject'),
                'user' => $user,
                'user_agent' => $event->getRequest()->headers->get('User-Agent'),
                'login_at' => new \DateTime(),
                'ip' => $event->getRequest()->getClientIp(),
            ]
        );
    }

    /**
     * @param TwoFactorAuthenticationEvent $event
     *
     * @throws \Exception
     */
    public function onTwoFactorLogin(TwoFactorAuthenticationEvent $event)
    {
        if (!$this->loginEmail) {
            return;
        }

        /** @var User $user */
        $user = $event->getToken()->getUser();

        $this->mail->send(
            $this->translator->trans('user.mail.login_subject'),
            $user->getEmail(),
            '@User/mail/user/login',
            [
                'subject' => $this->translator->trans('user.mail.login_subject'),
                'user' => $user,
                'user_agent' => $event->getRequest()->headers->get('User-Agent'),
                'login_at' => new \DateTime(),
                'ip' => $event->getRequest()->getClientIp(),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            UserEvents::SOCIAL_REGISTRATION => 'onUserSocialRegistered',
            UserEvents::LOST_PASSWORD => 'onUserLostPassword',
            UserEvents::REGISTRATION => 'onUserRegistered',
            SecurityEvents::INTERACTIVE_LOGIN => 'onLogin',
            TwoFactorAuthenticationEvents::SUCCESS => 'onTwoFactorLogin'
        ];
    }
}
