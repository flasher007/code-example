<?php
namespace Comment\Model\DomainModel\Interfaces\ProductVote;

use Comment\Model\DomainModel\Interfaces\ProductComment\Basic as ProductCommentBasic;

interface ProductComment extends Basic
{
    /**
     * Установить связанную сущность \Comment\Model\DomainModel\ProductComment
     * @param ProductCommentBasic $productComment
     * @return $this
     */
    public function setComment(ProductCommentBasic $productComment);

    /**
     * Получить поле product_comment
     * @return \Comment\Model\DomainModel\ProductComment
     */
    public function getComment();

}