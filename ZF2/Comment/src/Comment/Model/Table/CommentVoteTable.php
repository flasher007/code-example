<?php
namespace Comment\Model\Table;

use Basic\Db\Table;

/**
 * @method \Comment\Model\Select\CommentVoteSelect getSelect()
 */
abstract class CommentVoteTable extends Table
{
    protected $table = 'comment_vote';
    protected $resultObjectPrototype = '\Shop\Model\DomainModel\CommentVote';
    protected $selectClassName = '\Shop\Model\Select\CommentVoteSelect';

    /**
     * Выбирает комментарии коллекции
     * @param int $userId - идентификатор пользователя
     * @param int|array $commentIds - один или несколько идентификаторов комментариев для которых найти голоса пользователя
     * @return \Basic\Db\ResultSet|\Comment\Model\DomainModel\Interfaces\ProductCommentVote\Basic[]
     */
    public function fetchUserVotesOfComments($userId, $commentIds)
    {
        if (is_array($commentIds) && empty($commentIds)) {
            return $this->returnEmptyResultSet();
        }
        $select = $this->getSelect();
        $select->whereCommentId($commentIds);
        $select->whereUserId($userId);

        $result = $this->fetchAll();
        return $result;
    }

    /**
     * Выбирает комментарии коллекции
     * @param int|array $commentIds - один или несколько идентификаторов комментариев для которых найти голоса пользователя
     * @return \Basic\Db\ResultSet|\Shop\Model\DomainModel\Interfaces\ProductCommentVote\Basic[]
     */
    public function fetchAllOfComments($commentIds)
    {
        if (is_array($commentIds) && empty($commentIds)) {
            return $this->returnEmptyResultSet();
        }
        $select = $this->getSelect();
        $select->whereCommentId($commentIds);

        $result = $this->fetchAll();
        return $result;
    }

    /**
     * Возвращает все комментарии пользователя
     * @param int    $userId
     * @return \Shop\Model\DomainModel\Interfaces\ProductCommentVote\Basic[]|\Basic\Db\ResultSet
     */
    public function fetchAllByUser($userId)
    {
        $select = $this->getSelect();
        $select->whereUserId($userId);

        return $this->fetchAll();
    }
}