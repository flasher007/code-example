<?php
namespace Comment\Model\DomainModel\Interfaces\CollectionVote;


interface Basic
{
    /**
     * Установить поле id
     * @param $id
     * @return $this
     */
    public function setId($id);

    /**
     * Получить поле id
     * @return integer
     */
    public function getId();

    /**
     * Установить поле collection_id
     * @param $collectionId
     * @return $this
     */
    public function setCollectionId($collectionId);

    /**
     * Получить поле collection_id
     * @return integer
     */
    public function getCollectionId();

    /**
     * Установить поле user_id
     * @param $userId
     * @return $this
     */
    public function setUserId($userId);

    /**
     * Получить поле user_id
     * @return integer
     */
    public function getUserId();

    /**
     * Установить поле estimate
     * @param $estimate
     * @return $this
     */
    public function setEstimate($estimate);

    /**
     * Получить поле estimate
     * @return integer
     */
    public function getEstimate();

    /**
     * Установить поле comment_id
     * @param $commentId
     * @return $this
     */
    public function setCommentId($commentId);

    /**
     * Получить поле comment_id
     * @return integer
     */
    public function getCommentId();

}