<?php
namespace Comment\Model\DomainModel\Interfaces\CollectionVote;

use Comment\Model\DomainModel\Interfaces\CollectionComment\Basic as CollectionCommentBasic;

interface CollectionComment extends Basic
{
    /**
     * Установить связанную сущность \Comment\Model\DomainModel\CollectionComment
     * @param CollectionCommentBasic $collectionComment
     * @return $this
     */
    public function setComment(CollectionCommentBasic $collectionComment);

    /**
     * Получить поле collection_comment
     * @return \Comment\Model\DomainModel\CollectionComment
     */
    public function getComment();

}