<?php
namespace Comment\Model\Table;

use Comment\Model\Select\CollectionCommentSelect;
use User\Model\Select\UserSelect;

/**
 * @method \Comment\Model\Select\CollectionVoteSelect getSelect()
 */
class CollectionVoteTable extends VoteTable
{
    protected $table = 'collection_vote';
    protected $resultObjectPrototype = '\Comment\Model\DomainModel\CollectionVote';
    protected $selectClassName = '\Comment\Model\Select\CollectionVoteSelect';

    /**
     * @return \Comment\Model\Select\CommentSelect
     */
    public function getCommentSelect()
    {
        return new CollectionCommentSelect();
    }
}