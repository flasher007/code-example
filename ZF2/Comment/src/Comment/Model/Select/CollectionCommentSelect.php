<?php
namespace Comment\Model\Select;


class CollectionCommentSelect extends CommentSelect
{
    /**
     * @var string|\Zend\Db\Sql\TableIdentifier
     */
    protected $table = 'collection_comment';



    /**
     * добавляет к выборке условие на поле collection_id.
     * Является алиасом к whereEntityId()
     * @param int|array $collectionId - айдиха продукта
     * @return $this
     */
    public function whereCollectionId($collectionId)
    {
        $table = $this->getTableName();
        $this->where(array($table . '.collection_id' => $collectionId));

        return $this;
    }


    /**
     * добавляет к выборке условие на ключ к таблице, к которой относятся комментарии
     * @param int|array $entityId - айдиха сущности к которой относятся комментарии
     * @return $this
     */
    public function whereEntityId($entityId)
    {
        $this->whereCollectionId($entityId);
        return $this;
    }
}