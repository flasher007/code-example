<?php

namespace Comment\Controller;


/**
 * Базовый контроллер для всех комментариев в системе
*/
class ProductController extends IndexController
{

    /**
     * @return \Comment\Service\ProductCommentService
     */
    protected function getCommentService()
    {
        return $this->getServiceLocator()->get('comment.product_comment_service');
    }

    /**
     * Возвращает имя шаблона списка
     * @return string
     */
    protected function getListTemplate()
    {
        return 'comment/product/comment-list';
    }

    /**
     * Возвращает имя шаблона формы
     * @return string
     */
    protected function getFormTemplate()
    {
        return 'comment/product/comment-form';
    }

    /**
     * Возвращает имя контроллера в пути к странице
     * @return string
     */
    public function getPathToAction()
    {
        return '/comment/product';
    }
}
