<?php
namespace Comment\Model\Select;

use Basic\Db\Select;

abstract class CommentVoteSelect extends Select
{
    /**
     * @var string|\Zend\Db\Sql\TableIdentifier
     */
    protected $table = 'comment_vote';

    /**
     * Добавляет условие на отсеивание по полю комментария
     * @param int|array $commentId - айдиха или несколько айдих комментариев
     * @return $this
     */
    abstract public function whereCommentId($commentId);


    /**
     * Добавляет условие на пользователя
     * @param int|array $userId - айдиха или несколько айдих пользователей
     * @return $this
     */
    public function whereUserId($userId)
    {
        $table = $this->getTableName();
        $this->where(array($table . '.' . 'user_id' => $userId));
        return $this;
    }
}