<?php
namespace Comment\Model\DomainModel\Interfaces\ProductCommentVote;

use Comment\Model\DomainModel\Interfaces\ProductComment\Basic as ProductCommentBasic;


interface Product extends Basic
{
    /**
     * Установить связанную сущность \Comment\Model\DomainModel\ProductComment
     * @param ProductCommentBasic $productComment
     * @return $this
     */
    public function setProductComment(ProductCommentBasic $productComment);

    /**
     * Получить поле product_comment
     * @return \Comment\Model\DomainModel\ProductComment
     */
    public function getProductComment();

}