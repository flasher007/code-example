<?php
namespace Comment\Model\DomainModel;

use Comment\Model\DomainModel\Interfaces\ProductComment\Basic as ProductCommentBasic;
use User\Model\User as UserBasic;

class ProductCommentVote extends CommentVote
{

    /**
     * Установить поле product_comment_id
     * @param $productCommentId
     * @return $this
     */
    public function setProductCommentId($productCommentId)
    {
        $this->setFieldValue('product_comment_id', $productCommentId);
        return $this;
    }

    /**
     * Получить поле product_comment_id
     * @return integer
     */
    public function getProductCommentId()
    {
        return $this->getFieldValue('product_comment_id');
    }


    /**
     * Установить связанную сущность \Shop\Model\DomainModel\ProductComment
     * @param ProductCommentBasic $productComment
     * @return $this
     */
    public function setProductComment(ProductCommentBasic $productComment)
    {
        $this->setFieldValue('product_comment', $productComment);
        return $this;
    }

    /**
     * Получить поле product_comment
     * @return \Shop\Model\DomainModel\ProductComment
     */
    public function getProductComment()
    {
        return $this->getFieldValue('product_comment');
    }

    /**
     * Установить связанную сущность \User\Model\User
     * @param UserBasic $user
     * @return $this
     */
    public function setUser(UserBasic $user)
    {
        $this->setFieldValue('user', $user);
        return $this;
    }

    /**
     * Получить поле user
     * @return \Shop\Model\DomainModel\User
     */
    public function getUser()
    {
        return $this->getFieldValue('user');
    }

    protected function setFields()
    {
        parent::setFields();
        $this->setIntField('product_comment_id', null, true);
    }

    protected function setRelatedEntities()
    {
        $this->setRelatedEntity('product_comment', '\Shop\Model\DomainModel\ProductComment');
        $this->setRelatedEntity('user', '\User\Model\User');
    }

    /**
     * Установить поле ссылку на коментарий
     * @param $commentId
     * @return $this
     */
    public function setCommentId($commentId)
    {
        return $this->setProductCommentId($commentId);
    }

    /**
     * Получить поле product_comment_id
     * @return integer
     */
    public function getCommentId()
    {
        return $this->getProductCommentId();
    }
}