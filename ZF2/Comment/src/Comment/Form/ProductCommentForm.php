<?php
namespace Comment\Form;

use Basic\Form\Form;
use Zend\Form\Element;

class ProductCommentForm extends Form
{

    public function __construct($name = 'product_comment')
    {
        parent::__construct($name);


        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'style' => 'width:100%',
                'id' => 'name'
            ),
            'options' => array(
                'label' => 'Имя',
            ),
        ));
        $this->add(array(
            'name' => 'lname',
            'attributes' => array(
                'type' => 'text',
                'style' => 'width:100%',
                'id' => 'lname'
            ),
            'options' => array(
                'label' => 'Фамилия',
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'style' => 'width:100%',
                'id' => 'email'
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));

        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            ),
        ));
        $this->add(array(
            'name' => 'estimate',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'estimate'
            ),
        ));

        $this->add(array(
            'name' => 'comment',
            'attributes' => array(
                'type' => 'textarea',
                'style' => 'width:100%; height:70px',
                'id' => 'comment_form_comment'
            ),
            'options' => array(
                'label' => '',
            ),
        ));

        $captchaAdapter = \Zend\Captcha\Factory::factory(array(
            'class'   => 'image',
            'options' => array(
                'font' => __DIR__ . '/../../../../../public/css/font/arial.ttf',
                'width' => 100,
                'wordlen' => 4
            )
        ));

        $captcha = new Element\Captcha('captcha');
        $captcha->setCaptcha($captchaAdapter);
        $captcha->setOptions(array('label' => 'Введите код с картинки.'));
        $captcha->setAttribute('id', 'captcha');
        $this->add($captcha);

        $this->add(array(
            'name' => 'sendBtn',
            'type'  => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Отправить комментарий',
                'class' => 'btn btn-primary'
            ),
        ));

    }

    public function changeFormByUser(\User\Model\User $user = null)
    {
        if ($user) {
            $this->remove('captcha');
            $this->remove('email');
            if ($user->name) {
                $this->remove('name');
            }

            if ($user->lname) {
                $this->remove('lname');
            }
        }
    }
}
