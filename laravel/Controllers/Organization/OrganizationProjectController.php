<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Resources\Project\ProjectCardResource;
use App\Services\Organization\OrganizationService;

class OrganizationProjectController extends Controller
{
    // GET /organizations/{id}/projects
    public function index(int $id, OrganizationService $service)
    {
        return ProjectCardResource::collection($service->projects($id));
    }
}
