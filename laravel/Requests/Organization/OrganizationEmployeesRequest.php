<?php

namespace App\Http\Requests\Organization;

use App\Http\Requests\Search\SearchUserRequest;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationEmployeesRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @param SearchUserRequest $request
     * @return mixed
     */
    public function rules(SearchUserRequest $request)
    {
        return collect($request->rules())
            ->mapWithKeys(function ($value, $key) {
                return ["filter.$key" => $value];
            })
            ->merge([
                'filter' => 'array',
            ])
            ->toArray();
    }

}
