<?php

namespace App\Models;

use App\Services\Paginate\PaginateService;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $organization_id
 * @property bool $is_visible
 * @property string $text
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 *
 * @property User $author
 * @property Organization $organization
 */
class OrganizationFeedback extends Model
{
    protected $fillable = ['text'];

    protected $casts = [
        'is_visible' => 'bool',
    ];

    /**
     * OrganizationFeedback constructor.
     * @param array $attributes
     * @throws \App\Exceptions\Pagination\PaginationException
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setPerPage(PaginateService::getPerPage(__CLASS__));
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
