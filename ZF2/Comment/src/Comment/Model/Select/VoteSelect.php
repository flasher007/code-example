<?php
namespace Comment\Model\Select;

use Basic\Db\Select;

abstract class VoteSelect extends Select
{
    /**
     * @var string|\Zend\Db\Sql\TableIdentifier
     */
    protected $table = 'product_vote';

    /**
     * Создает условие по запросу
     * @param \Comment\Model\Request\VoteListRequest $request
     * @return $this
     */
    public function formConditionByRequest($request)
    {
        if ($request->comment_id) {
            $this->whereCommentId($request->comment_id);
        }
        if ($request->entity_id) {
            $this->whereEntityId($request->entity_id);
        }
        if ($request->sort) {
            $this->order($request->sort . ' ' . $request->direction);
        }
        if (!empty($request->countPerPage)) {
            $this->limit($request->countPerPage);
            if (!empty($request->page)) {
                $this->offset(($request->page-1) * $request->countPerPage);
            }
        }

        return $this;
    }

    /**
     * добавляет к выборке условие на поле product_id
     * @param int|array $entityId - один id продукта или массив айдишников продуктов
     * @return $this
     */
    abstract public function whereEntityId($entityId);


    /**
     * добавляет к выборке условие на поле comment_id
     * @param int|array $commentId - один id продукта или массив айдишников продуктов
     * @return $this
     */
    public function whereCommentId($commentId)
    {
        $table = $this->getTableName();
        $this->where(array($table . '.comment_id' => $commentId));

        return $this;
    }

    /**
     * добавляет к выборке условие на поле user_id
     * @param int|array $userId - один id продукта или массив айдишников пользователей
     * @return $this
     */
    public function whereUserId($userId)
    {
        $table = $this->getTableName();
        $this->where(array($table . '.user_id' => $userId));

        return $this;
    }

    /**
     * добавляет к выборке условие на поле email
     * @param string $email - запрошенный ящик
     * @return $this
     */
    public function whereEmail($email)
    {
        $table = $this->getTableName();
        $this->where(array($table . '.email' => $email));

        return $this;
    }

    /**
     * Join таблицы комментариев
     * @param \Comment\Model\Select\CommentSelect $commentSelect
     * @param string $join - метод соединения
     * @return $this
     */
    abstract public function joinComment(CommentSelect $commentSelect = null, $join = self::JOIN_LEFT);

}