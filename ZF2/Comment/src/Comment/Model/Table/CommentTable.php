<?php
namespace Comment\Model\Table;

use Basic\Db\Table;

/**
 * @method \Comment\Model\Select\CommentSelect getSelect()
 */
abstract class CommentTable extends Table
{
    protected $table = 'comment';
    protected $resultObjectPrototype = '\Comment\Model\DomainModel\Comment';
    protected $selectClassName = '\Comment\Model\Select\CommentSelect';

    /**
     * Возвращает все комментарии оставленные к заданной сущности
     * @param int    $entityId - идентификатор сущности, к которому нужно получить комментарии
     * @return \Comment\Model\DomainModel\Interfaces\Comment\Basic[]
     */
    public function fetchAllByEntity($entityId)
    {
        $select = $this->getSelect();
        $select->whereEntityId($entityId);

        return $this->fetchAll();
    }

    /**
     * Возвращает все комментарии пользователя
     * @param int    $userId - идентификатор пользователя, комментарии которого нужно найти
     * @return \Comment\Model\DomainModel\Interfaces\Comment\Basic[]
     */
    public function fetchAllByUser($userId)
    {
        $select = $this->getSelect();
        $select->whereUserId($userId);

        return $this->fetchAll();
    }

}