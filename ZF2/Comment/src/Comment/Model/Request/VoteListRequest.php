<?php
namespace Comment\Model\Request;

use Basic\Model\PagedWithSortRequest;

class VoteListRequest extends PagedWithSortRequest
{
    public $entity_id;
    public $comment_id;

    /**
     * Распихивает данные по полям. Возвращает остаток данных, которые не удалось распределить
     * @param array $data
     * @return array
     */
    public function exchangeArray($data)
    {
        $data = parent::exchangeArray($data);

        $this->exchangeField('entity_id' ,$data);
        $this->exchangeField('comment_id' ,$data);

        return $data;
    }

    /**
     * Возвращает копию данных модели в виде массива
     * @param bool $withJoinedTables
     * @return array
     */
    public function getArrayCopy($withJoinedTables = false)
    {
        $data = parent::getArrayCopy($withJoinedTables);

        $this->getFieldCopy('entity_id', $data);
        $this->getFieldCopy('comment_id', $data);

        return $data;
    }

    public function getInputFilter()
    {
        if ($this->filterIsInit == false) {
            parent::getInputFilter();

            $this->addFilter(array(
                'name'     => 'entity_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
                'validators' => array(
                    array(
                        'name' => 'GreaterThan',
                        'options' => array(
                            'min' => 0
                        )
                    ),
                ),
            ));

            $this->addFilter(array(
                'name'     => 'comment_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
                'validators' => array(
                    array(
                        'name' => 'GreaterThan',
                        'options' => array(
                            'min' => 0
                        )
                    ),
                ),
            ));

            $this->inputFilter->remove('count_per_page');

            $this->addFilter(array(
                'name'     => 'count_per_page',
                'filters'  => array(
                    array('name' => 'Int'),
                ),
                'validators' => array(
                    array(
                        'name' => 'GreaterThan',
                        'options' => array(
                            'min' => 0
                        )
                    ),
                    array(
                        'name' => 'LessThan',
                        'options' => array(
                            'max' => 1000
                        )
                    ),
                ),
            ));
        }

        return $this->inputFilter;
    }
}