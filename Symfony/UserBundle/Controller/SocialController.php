<?php

declare(strict_types=1);

/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace HypeCorp\Bundle\UserBundle\Controller;

use HypeCorp\Bundle\UserBundle\SocialAuth\Exception\EmailRequiredException;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class SocialController extends Controller
{
    /**
     * @param string $provider
     *
     * @throws \Exception
     *
     * @return RedirectResponse|Response
     */
    public function auth(string $provider)
    {
        $oauthDispatcher = $this->get('hypecorp.social_auth.dispatcher.chain_dispatcher');
        $loginUtil = $this->get('hypecorp.bundle.user.util.login_util');
        $factory = $this->get('hypecorp.social_auth.client.factory');
        $settingManager = $this->get('max107.bundle.setting_manager');
        $oauthClient = $factory->createClient($provider);

        if (false === $settingManager->getBoolean(sprintf('social_auth__%s_is_enabled', $provider))) {
            throw $this->createNotFoundException();
        }

        try {
            $result = $oauthDispatcher->process($provider, $oauthClient);
        } catch (IdentityProviderException | EmailRequiredException $e) {
            return $this->render('@User/user/social/auth/error.html.twig', [
                'error' => $e,
                'provider' => $provider,
            ]);
        }

        if ($result instanceof RedirectResponse) {
            return $result;
        }

        $loginUtil->loginUser($result);

        return $this->redirectToRoute('dashboard');
    }
}
