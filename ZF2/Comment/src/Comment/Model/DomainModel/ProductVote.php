<?php
namespace Comment\Model\DomainModel;

use Basic\Model\DomainModel;
use Shop\Model\DomainModel\Interfaces\Product\Basic as ProductBasic;
use User\Model\User as UserBasic;
use Comment\Model\DomainModel\Interfaces\ProductComment\Basic as ProductCommentBasic;

class ProductVote extends DomainModel
{
    /**
     * Установить поле id
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->setFieldValue('id', $id);
        return $this;
    }

    /**
     * Получить поле id
     * @return integer
     */
    public function getId()
    {
        return $this->getFieldValue('id');
    }

    /**
     * Установить поле product_id
     * @param $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->setFieldValue('product_id', $productId);
        return $this;
    }

    /**
     * Получить поле product_id
     * @return integer
     */
    public function getProductId()
    {
        return $this->getFieldValue('product_id');
    }

    /**
     * Установить поле user_id
     * @param $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->setFieldValue('user_id', $userId);
        return $this;
    }

    /**
     * Получить поле user_id
     * @return integer
     */
    public function getUserId()
    {
        return $this->getFieldValue('user_id');
    }

    /**
     * Установить поле estimate
     * @param $estimate
     * @return $this
     */
    public function setEstimate($estimate)
    {
        $this->setFieldValue('estimate', $estimate);
        return $this;
    }

    /**
     * Получить поле estimate
     * @return integer
     */
    public function getEstimate()
    {
        return $this->getFieldValue('estimate');
    }

    /**
     * Установить поле comment_id
     * @param $commentId
     * @return $this
     */
    public function setCommentId($commentId)
    {
        $this->setFieldValue('comment_id', $commentId);
        return $this;
    }

    /**
     * Получить поле comment_id
     * @return integer
     */
    public function getCommentId()
    {
        return $this->getFieldValue('comment_id');
    }

    /**
     * Установить поле email
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->setFieldValue('email', $email);
        return $this;
    }

    /**
     * Получить поле email
     * @return string
     */
    public function getEmail()
    {
        return $this->getFieldValue('email');
    }

    /**
     * Установить связанную сущность \Shop\Model\DomainModel\Product
     * @param ProductBasic $product
     * @return $this
     */
    public function setProduct(ProductBasic $product)
    {
        $this->setFieldValue('product', $product);
        return $this;
    }

    /**
     * Получить поле product
     * @return \Shop\Model\DomainModel\Product
     */
    public function getProduct()
    {
        return $this->getFieldValue('product');
    }

    /**
     * Установить связанную сущность \User\Model\User
     * @param UserBasic $user
     * @return $this
     */
    public function setUser(UserBasic $user)
    {
        $this->setFieldValue('user', $user);
        return $this;
    }

    /**
     * Получить поле user
     * @return \User\Model\User
     */
    public function getUser()
    {
        return $this->getFieldValue('user');
    }

    /**
     * Установить связанную сущность \Comment\Model\DomainModel\Comment
     * @param ProductCommentBasic $comment
     * @return $this
     */
    public function setComment(ProductCommentBasic $comment)
    {
        $this->setFieldValue('comment', $comment);
        return $this;
    }

    /**
     * Получить поле comment
     * @return \Comment\Model\DomainModel\ProductComment
     */
    public function getComment()
    {
        return $this->getFieldValue('comment');
    }

    protected function setFields()
    {
        $this->setIntField('id', null, true);
        $this->setIntField('product_id', null, true);
        $this->setIntField('user_id', null, true);
        $this->setIntField('estimate', null, true);
        $this->setIntField('comment_id', null, false);
        $this->setStringField('email', null, false, true, 1, 255);
    }

    protected function setRelatedEntities()
    {
        $this->setRelatedEntity('product', '\Shop\Model\DomainModel\Product');
        $this->setRelatedEntity('user', '\User\Model\User');
        $this->setRelatedEntity('comment', '\Comment\Model\DomainModel\ProductComment', 'product_comment');
    }

    public function exchangeArray($data)
    {
        $data = parent::exchangeArray($data);
        if (array_key_exists('entity_id', $data)) {
            $this->setFieldValue('product_id', $data['entity_id']);
        }
        return $data;
    }
}