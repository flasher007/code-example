<?php
namespace Comment\Model\DomainModel\Interfaces\CollectionCommentVote;

use Comment\Model\DomainModel\Interfaces\CollectionComment\Basic as CollectionCommentBasic;


interface Collection extends Basic
{
    /**
     * Установить связанную сущность \Comment\Model\DomainModel\CollectionComment
     * @param CollectionCommentBasic $collectionComment
     * @return $this
     */
    public function setCollectionComment(CollectionCommentBasic $collectionComment);

    /**
     * Получить поле collection_comment
     * @return \Comment\Model\DomainModel\CollectionComment
     */
    public function getCollectionComment();

}