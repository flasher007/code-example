<?php
namespace Comment\Model\Select;

use Basic\Db\Select;
use User\Model\Select\UserSelect;

class ProductCommentSelect extends CommentSelect
{
    /**
     * @var string|\Zend\Db\Sql\TableIdentifier
     */
    protected $table = 'product_comment';



    /**
     * добавляет к выборке условие на поле product_id.
     * Является алиасом к whereEntityId()
     * @param int|array $productId - айдиха продукта
     * @return $this
     */
    public function whereProductId($productId)
    {
        $table = $this->getTableName();
        $this->where(array($table . '.product_id' => $productId));

        return $this;
    }


    /**
     * добавляет к выборке условие на ключ к таблице, к которой относятся комментарии
     * @param int|array $entityId - айдиха сущности к которой относятся комментарии
     * @return $this
     */
    public function whereEntityId($entityId)
    {
        $this->whereProductId($entityId);
        return $this;
    }
}