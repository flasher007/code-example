<?php

namespace Comment\Service;


use Comment\Form\CollectionCommentForm;
use Comment\Model\DomainModel\CollectionComment;
use Comment\Model\DomainModel\CollectionCommentVote;
use Comment\Model\DomainModel\CollectionVote;
use User\Model\Table\UserTable;
use \Basic\Grid\Column;
use User\Model\User;

/**
 * Базовый сервис комментариев
 * Class CommentService
 * @package Comment\Service
 */
class CollectionCommentService extends ProductCommentService
{
    /**
     * Объект таблицы работающий с комментариями
     *
     * @var \Comment\Model\Table\CollectionCommentTable
     */
    protected $commentTable = 'Comment\Model\Table\CollectionCommentTable';

    /**
     * Объект таблицы работающий с оценками комментариев
     *
     * @var \Comment\Model\Table\CollectionCommentVoteTable
     */
    protected $commentVoteTable = 'Comment\Model\Table\CollectionCommentVoteTable';

    /**
     * Объект таблицы работающий с оценками
     *
     * @var \Comment\Model\Table\VoteTable
     */
    protected $voteTable = 'Comment\Model\Table\CollectionVoteTable';


    /**
     * Возвращает форму для добавления комментариев к заданной сущности от заданного пользователя
     * @param int|string $entityId - сущность к которой будут добавляться комментарии
     * @param \ZfcUser\Entity\User $user - пользователь для которого создается форма
     * @return \Comment\Form\CollectionCommentForm
     */
    public function getCommentForm($entityId, \ZfcUser\Entity\User $user = null)
    {
        $form = new CollectionCommentForm();
        $voteTable = $this->getVoteTable();
        $userModel = null;
        if ($user) {
            $productVote = $voteTable->fetchUserVoteToEntity($user->getId(), $entityId);

            if ($productVote) {
                return null;
            }
            $userModel = new User();
            $userModel->exchangeFromZfcUser($user);
        }
        $form->changeFormByUser($userModel);

        return $form;
    }

    /**
     * Возвращает пустую модель отзыва на комментарий
     * @return \Comment\Model\DomainModel\CollectionCommentVote
     */
    function getCommentVoteModel()
    {
        return new CollectionCommentVote();
    }

    /**
     * Возвращает пустую модель комментария
     * @return \Comment\Model\DomainModel\CollectionComment
     */
    function getCommentModel()
    {
        return new CollectionComment();
    }

    /**
     * Возвращает пустую модель комментария
     * @return \Comment\Model\DomainModel\CollectionVote
     */
    function getVoteModel()
    {
        return new CollectionVote();
    }
}