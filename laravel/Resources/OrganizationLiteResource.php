<?php

namespace App\Http\Resources\Organization;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin \App\Models\Organization
 */
class OrganizationLiteResource extends Resource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'shortTitle' => $this->short_title,
            'website' => $this->website,
            'avatar' => null,
        ];
    }
}
