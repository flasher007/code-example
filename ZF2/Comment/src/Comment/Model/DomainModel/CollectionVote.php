<?php
namespace Comment\Model\DomainModel;

use Basic\Model\DomainModel;
use User\Model\Collection as CollectionBasic;
use User\Model\User as UserBasic;
use Comment\Model\DomainModel\Interfaces\CollectionComment\Basic as CollectionCommentBasic;

class CollectionVote extends DomainModel
{
    /**
     * Установить поле id
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->setFieldValue('id', $id);
        return $this;
    }

    /**
     * Получить поле id
     * @return integer
     */
    public function getId()
    {
        return $this->getFieldValue('id');
    }

    /**
     * Установить поле collection_id
     * @param $collectionId
     * @return $this
     */
    public function setCollectionId($collectionId)
    {
        $this->setFieldValue('collection_id', $collectionId);
        return $this;
    }

    /**
     * Получить поле collection_id
     * @return integer
     */
    public function getCollectionId()
    {
        return $this->getFieldValue('collection_id');
    }

    /**
     * Установить поле user_id
     * @param $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->setFieldValue('user_id', $userId);
        return $this;
    }

    /**
     * Получить поле user_id
     * @return integer
     */
    public function getUserId()
    {
        return $this->getFieldValue('user_id');
    }

    /**
     * Установить поле estimate
     * @param $estimate
     * @return $this
     */
    public function setEstimate($estimate)
    {
        $this->setFieldValue('estimate', $estimate);
        return $this;
    }

    /**
     * Получить поле estimate
     * @return integer
     */
    public function getEstimate()
    {
        return $this->getFieldValue('estimate');
    }

    /**
     * Установить поле comment_id
     * @param $commentId
     * @return $this
     */
    public function setCommentId($commentId)
    {
        $this->setFieldValue('comment_id', $commentId);
        return $this;
    }

    /**
     * Получить поле comment_id
     * @return integer
     */
    public function getCommentId()
    {
        return $this->getFieldValue('comment_id');
    }

    /**
     * Установить поле email
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->setFieldValue('email', $email);
        return $this;
    }

    /**
     * Получить поле email
     * @return string
     */
    public function getEmail()
    {
        return $this->getFieldValue('email');
    }

    /**
     * Установить связанную сущность \User\Model\Collection
     * @param CollectionBasic $collection
     * @return $this
     */
    public function setCollection(CollectionBasic $collection)
    {
        $this->setFieldValue('collection', $collection);
        return $this;
    }

    /**
     * Получить поле collection
     * @return \User\Model\Collection
     */
    public function getCollection()
    {
        return $this->getFieldValue('collection');
    }

    /**
     * Установить связанную сущность \User\Model\User
     * @param UserBasic $user
     * @return $this
     */
    public function setUser(UserBasic $user)
    {
        $this->setFieldValue('user', $user);
        return $this;
    }

    /**
     * Получить поле user
     * @return \User\Model\User
     */
    public function getUser()
    {
        return $this->getFieldValue('user');
    }

    /**
     * Установить связанную сущность \Comment\Model\DomainModel\Comment
     * @param CollectionCommentBasic $comment
     * @return $this
     */
    public function setComment(CollectionCommentBasic $comment)
    {
        $this->setFieldValue('comment', $comment);
        return $this;
    }

    /**
     * Получить поле comment
     * @return \Comment\Model\DomainModel\CollectionComment
     */
    public function getComment()
    {
        return $this->getFieldValue('comment');
    }

    protected function setFields()
    {
        $this->setIntField('id', null, true);
        $this->setIntField('collection_id', null, true);
        $this->setIntField('user_id', null, true);
        $this->setIntField('estimate', null, true);
        $this->setIntField('comment_id', null, false);
        $this->setStringField('email', null, false, true, 1, 255);
    }

    protected function setRelatedEntities()
    {
        $this->setRelatedEntity('collection', '\User\Model\Collection');
        $this->setRelatedEntity('user', '\User\Model\User');
        $this->setRelatedEntity('comment', '\Comment\Model\DomainModel\CollectionComment', 'collection_comment');
    }

    public function exchangeArray($data)
    {
        $data = parent::exchangeArray($data);
        if (array_key_exists('entity_id', $data)) {
            $this->setFieldValue('collection_id', $data['entity_id']);
        }
        return $data;
    }
}