<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\Organization\OrganizationDestroyEmployeeRequest;
use App\Http\Requests\Organization\OrganizationEmployeesRequest;
use App\Http\Resources\Search\SearchUsersResource;
use App\Services\Organization\OrganizationService;

class OrganizationEmployeeController extends Controller
{
    /**
     * GET /organizations/{id}/employees
     *
     * @param int $id
     * @param OrganizationEmployeesRequest $request
     * @param OrganizationService $service
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(int $id, OrganizationEmployeesRequest $request, OrganizationService $service)
    {
        return SearchUsersResource::collection($service->getEmployees($id, $request->validated()));
    }

    /**
     * DELETE /organizations/{id}/employees/{userId}
     *
     * @param int $id
     * @param int $userId
     * @param OrganizationDestroyEmployeeRequest $request
     * @param OrganizationService $service
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\Organization\EmployeeNotFoundException
     */
    public function destroy(int $id, int $userId, OrganizationDestroyEmployeeRequest $request, OrganizationService $service)
    {
        $service->destroyEmployee($id, $userId);

        return response('', 204);
    }
}
