<?php
namespace Comment\Model\DomainModel\Interfaces\ProductCommentVote;


interface Basic
{
    /**
     * Установить поле id
     * @param $id
     * @return $this
     */
    public function setId($id);

    /**
     * Получить поле id
     * @return integer
     */
    public function getId();

    /**
     * Установить поле product_comment_id
     * @param $productCommentId
     * @return $this
     */
    public function setProductCommentId($productCommentId);

    /**
     * Получить поле product_comment_id
     * @return integer
     */
    public function getProductCommentId();

    /**
     * Установить поле user_id
     * @param $userId
     * @return $this
     */
    public function setUserId($userId);

    /**
     * Получить поле user_id
     * @return integer
     */
    public function getUserId();

    /**
     * Установить поле is_positive
     * @param $isPositive
     * @return $this
     */
    public function setIsPositive($isPositive);

    /**
     * Получить поле is_positive
     * @return boolean
     */
    public function getIsPositive();

}