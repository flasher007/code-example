<?php
namespace Comment\Model\Table;


/**
 * @method \Comment\Model\Select\CollectionCommentSelect getSelect()
 */
class CollectionCommentTable extends CommentTable
{
    protected $table = 'collection_comment';
    protected $resultObjectPrototype = '\Comment\Model\DomainModel\CollectionComment';
    protected $selectClassName = '\Comment\Model\Select\CollectionCommentSelect';

    /**
     * Удаляет в корзину записи, из связанных таблиц. В каждом потомке необходимо прописывать код удаления
     * @param int $id              - ид удалямой записи
     * @param int $ownerId         - владелец, удаливший запись
     * @param int $operationNumber - номер операции. Если null - выберется автоматом новый номер
     * @return bool - успех операции
     */
    protected function deleteRelatedEntities($id, $ownerId = null, $operationNumber = null)
    {
        $table = new CollectionCommentVoteTable();
        $items = $table->fetchAllOfComments($id);
        $res = $this->deleteItemsOfRelatedEntity($table, $items, $ownerId, $operationNumber);
        if (!$res) {
            return false;
        }

        return true;
    }
}