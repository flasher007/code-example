<?php
namespace Comment\Model\Table;


/**
 * @method \Comment\Model\Select\ProductCommentVoteSelect getSelect()
 */
class ProductCommentVoteTable extends CommentVoteTable
{
    protected $table = 'product_comment_vote';
    protected $resultObjectPrototype = '\Comment\Model\DomainModel\ProductCommentVote';
    protected $selectClassName = '\Comment\Model\Select\ProductCommentVoteSelect';
}