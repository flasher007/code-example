<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\Organization\OrganizationFeedbackStoreRequest;
use App\Http\Resources\Organization\OrganizationFeedbackResource;
use App\Services\Organization\OrganizationFeedbackService;

class OrganizationFeedbackController extends Controller
{
    // GET /organizations/{id}/feedback
    public function index(int $id, OrganizationFeedbackService $service)
    {
        return OrganizationFeedbackResource::collection($service->forOrganizationId($id));
    }

    // POST /organizations/{id}/feedback
    public function store(int $id, OrganizationFeedbackStoreRequest $request, OrganizationFeedbackService $service)
    {
        $feedback = $service->store($id, $request->user()->id, $request->input('text'));

        return new OrganizationFeedbackResource($feedback);
    }
}
