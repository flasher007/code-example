<?php

namespace App\Models;

use App\Models\User\WorkExperience;
use App\Services\Paginate\PaginateService;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use App\Models\EducationProgram\EducationProgram;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property int $responsible_id
 * @property string $title
 * @property string $short_title
 * @property array $types
 * @property int $legal_entity_type
 * @property string $description
 * @property string $address_registered
 * @property string $address_physical
 * @property string $person_name
 * @property string $person_phone
 * @property string $person_email
 * @property string $inn
 * @property string $kpp
 * @property string $website
 * @property string $email
 * @property string $phone
 * @property int $vacancies_count
 * @property int $employees_count
 * @property int $projects_count
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Support\Carbon $deleted_at
 *
 * @property \Illuminate\Database\Eloquent\Collection|User[] $admins
 * @property \Illuminate\Database\Eloquent\Collection|OrganizationFeedback[] $feedback
 * @property \Illuminate\Database\Eloquent\Collection|Project[] $projects
 * @property \Illuminate\Database\Eloquent\Collection|EducationProgram[] $educationPrograms
 * @property \Illuminate\Database\Eloquent\Collection|User[] $employees
 * @property User $responsible
 *
 * @property-read Pivot\OrganizationAdmin $organizationAdmin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vacancy[] $lastVacancies
 */
class Organization extends BaseModel
{
    use Filterable;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'inn',
        'kpp',
        'email',
        'phone',
        'title',
        'types',
        'website',
        'description',
        'person_name',
        'short_title',
        'person_email',
        'person_phone',
        'address_physical',
        'legal_entity_type',
        'address_registered',
    ];

    protected $casts = [
        'types' => 'json',
    ];

    /**
     * Organization constructor.
     * @param array $attributes
     * @throws \App\Exceptions\Pagination\PaginationException
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setPerPage(PaginateService::getPerPage(__CLASS__));
    }

    // Relations
    public function admins()
    {
        return $this->belongsToMany(User::class, 'organization_admins')
            ->withTimestamps()
            ->as('organizationAdmin')
            ->using(Pivot\OrganizationAdmin::class);
    }

    public function feedback()
    {
        return $this->hasMany(OrganizationFeedback::class);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function vacancies()
    {
        return $this->hasMany(Vacancy::class);
    }

    public function lastVacancies()
    {
        return $this->vacancies()
            ->orderBy('id', 'desc')
            ->limit(config('kpso.organization-last-vacancies-count'));
    }

    public function responsible()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function educationPrograms(): HasMany
    {
        return $this->hasMany(EducationProgram::class);
    }

    // Methods
    public function isAdmin(int $userId): bool
    {
        return in_array($userId, $this->admins->pluck('id')->toArray());
    }

    /**
     * @return BelongsToMany
     */
    public function employees(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'users_work_experience')
            ->wherePivot('is_approved_by_organization', true)
            ->distinct();
    }

    /**
     * @return HasOne|Builder
     */
    public function employeesCount()
    {
        return $this->hasOne(WorkExperience::class)
            ->where('is_approved_by_organization', true)
            ->selectRaw('organization_id, count(DISTINCT user_id) as count')
            ->groupBy('organization_id');
    }

    public function getEmployeesCountAttribute()
    {
        $this->loadMissing('employeesCount');
        return (int) optional($this->getRelation('employeesCount'))->count;
    }
}
