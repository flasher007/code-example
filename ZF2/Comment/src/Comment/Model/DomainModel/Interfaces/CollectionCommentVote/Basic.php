<?php
namespace Comment\Model\DomainModel\Interfaces\CollectionCommentVote;


interface Basic
{
    /**
     * Установить поле id
     * @param $id
     * @return $this
     */
    public function setId($id);

    /**
     * Получить поле id
     * @return integer
     */
    public function getId();

    /**
     * Установить поле collection_comment_id
     * @param $collectionCommentId
     * @return $this
     */
    public function setCollectionCommentId($collectionCommentId);

    /**
     * Получить поле collection_comment_id
     * @return integer
     */
    public function getCollectionCommentId();

    /**
     * Установить поле user_id
     * @param $userId
     * @return $this
     */
    public function setUserId($userId);

    /**
     * Получить поле user_id
     * @return integer
     */
    public function getUserId();

    /**
     * Установить поле is_positive
     * @param $isPositive
     * @return $this
     */
    public function setIsPositive($isPositive);

    /**
     * Получить поле is_positive
     * @return boolean
     */
    public function getIsPositive();

}