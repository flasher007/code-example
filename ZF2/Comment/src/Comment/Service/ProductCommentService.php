<?php

namespace Comment\Service;


use Comment\Form\ProductCommentForm;
use Comment\Model\DomainModel\ProductComment;
use Comment\Model\DomainModel\ProductCommentVote;
use Comment\Model\DomainModel\ProductVote;
use Comment\Model\Request\AddVoteRequest;
use Comment\Model\Table\ProductCommentTable;
use User\Model\Table\UserTable;
use Zend\Paginator\Paginator;
use \Basic\Grid\Column;
use User\Model\User;

/**
 * Базовый сервис комментариев
 * Class CommentService
 * @package Comment\Service
 */
class ProductCommentService extends CommentService
{
    /**
     * Объект таблицы работающий с комментариями
     *
     * @var \Comment\Model\Table\ProductCommentTable
     */
    protected $commentTable = 'Comment\Model\Table\ProductCommentTable';

    /**
     * Объект таблицы работающий с оценками комментариев
     *
     * @var \Comment\Model\Table\ProductCommentVoteTable
     */
    protected $commentVoteTable = 'Comment\Model\Table\ProductCommentVoteTable';

    /**
     * Объект таблицы работающий с оценками
     *
     * @var \Comment\Model\Table\VoteTable
     */
    protected $voteTable = 'Comment\Model\Table\ProductVoteTable';


    /**
     * Возвращает форму для добавления комментариев к заданной сущности от заданного пользователя
     * @param int|string $entityId - сущность к которой будут добавляться комментарии
     * @param \ZfcUser\Entity\User $user - пользователь для которого создается форма
     * @return \Comment\Form\ProductCommentForm
     */
    public function getCommentForm($entityId, \ZfcUser\Entity\User $user = null)
    {
        $form = new ProductCommentForm();
        $voteTable = $this->getVoteTable();
        $userModel = null;
        if ($user) {
            $productVote = $voteTable->fetchUserVoteToEntity($user->getId(), $entityId);

            if ($productVote) {
                return null;
            }
            $userModel = new User();
            $userModel->exchangeFromZfcUser($user);
        }
        $form->changeFormByUser($userModel);

        return $form;
    }

    /**
     * @param \Comment\Model\Request\AddVoteRequest $addVoteRequest
     * @param \Comment\Form\ProductCommentForm         $form - форма из которой пришли эти данные
     * @param \ZfcUser\Entity\User                  $user
     * @return bool
     */
    public function saveDataOfCommentForm($addVoteRequest, $form, \ZfcUser\Entity\User $user = null)
    {
        $commentTable =$this->getCommentTable();
        $voteTable = $this->getVoteTable();

        //подготавливаем проверки
        if ($user) {
            $name = $user->getName();
            $lName = $user->getLname();

            if (!empty($name)) {
                $addVoteRequest->removeUserName();
            }
            if (!empty($lName)) {
                $addVoteRequest->removeUserLName();
            }
            $form->remove('captcha');
            $addVoteRequest->removeEmail();
            $addVoteRequest->removeCaptcha();
            $addVoteRequest->setUserId($user->getId());
            $productVote = $voteTable->fetchUserVoteToEntity($addVoteRequest->getUserId(), $addVoteRequest->getEntityId());

            if ($productVote) {
                $form->setMessages(array('estimate' => array('Вы уже оставляли отзыв') ));
            }
        } else {
            $productVotes = $voteTable->fetchAllOfEntityByEmail($addVoteRequest->getEmail(), $addVoteRequest->getEntityId());

            if (count($productVotes) > 0) {
                $form->setMessages(array('email' => array('Пользователь с таким Email уже оставлял отзыв') ));
            }
        }

        $comment = $addVoteRequest->getComment();
        if (empty($comment)) {
            $addVoteRequest->removeUserNames();
        }

        $inpF = $addVoteRequest->getInputFilter();
        $form->setInputFilter($inpF);

        $form->setData($addVoteRequest->getArrayCopy());
        $msg = $form->getMessages();
        if ($form->isValid() && empty($msg)){
            //сохраняем данные
            if ($user) {
                $name = $user->getName();
                $lName = $user->getLname();

                if ((empty($name) || empty($lName))) {
                    $userTable = new UserTable();
                    /**
                     * @var \User\Model\User $userModel
                     */
                    $userModel = $userTable->find($user->getId());
                    if ($userModel) {
                        if (empty($name)) {
                            $userModel->name = $addVoteRequest->getName();
                        }
                        if (empty($lName)) {
                            $userModel->lname = $addVoteRequest->getLName();
                        }
                        $userModel->display_name = $userModel->getDisplayName();
                        $userTable->save($userModel);
                    } else {
                        return false;
                    }
                }
            }

            $commentId = null;
            if (!empty($comment)){
                $comment = $this->getCommentModel();
                $comment->exchangeArray($addVoteRequest->getArrayCopy());
                $comment->setUserName($addVoteRequest->getName());
                $comment->setUserLName($addVoteRequest->getLName());
                $commentId = $commentTable->save($comment);
            }
            $vote = $this->getVoteModel();
            $vote->exchangeArray($addVoteRequest->getArrayCopy());
            $vote->setCommentId($commentId);
            $voteTable->save($vote);

            return true;
        }

        return false;
    }

    /**
     * Возвращает пустую модель отзыва на комментарий
     * @return \Comment\Model\DomainModel\ProductCommentVote
     */
    function getCommentVoteModel()
    {
        return new ProductCommentVote();
    }

    /**
     * Возвращает пустую модель комментария
     * @return \Comment\Model\DomainModel\ProductComment
     */
    function getCommentModel()
    {
        return new ProductComment();
    }

    /**
     * Возвращает пустую модель комментария
     * @return \Comment\Model\DomainModel\ProductVote
     */
    function getVoteModel()
    {
        return new ProductVote();
    }
}