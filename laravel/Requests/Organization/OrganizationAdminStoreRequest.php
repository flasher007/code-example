<?php

namespace App\Http\Requests\Organization;

use App\Services\Organization\OrganizationService;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationAdminStoreRequest extends FormRequest
{
    public function authorize(OrganizationService $service): bool
    {
        $org = $service->findOrFail($this->route('id'));

        return $this->user()->can('attach-admin', $org);
    }

    public function rules(): array
    {
        return [
            'userId' => 'required|integer|min:1|exists:users,id',
        ];
    }
}
