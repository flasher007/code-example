<?php
namespace Comment\Model\Select;

use Basic\Db\Select;

class ProductVoteSelect extends VoteSelect
{
    /**
     * @var string|\Zend\Db\Sql\TableIdentifier
     */
    protected $table = 'product_vote';

    /**
     * Создает условие по запросу
     * @param \Comment\Model\Request\VoteListRequest $request
     * @return $this
     */
    public function formConditionByRequest($request)
    {
        parent::formConditionByRequest($request);

        return $this;
    }

    /**
     * добавляет к выборке условие на поле product_id
     * @param int|array $entityId - один id продукта или массив айдишников продуктов
     * @return $this
     */
    public function whereEntityId($entityId)
    {
        $table = $this->getTableName();
        $this->where(array($table . '.product_id' => $entityId));

        return $this;
    }

    /**
     * Join таблицы комментариев
     * @param \Comment\Model\Select\CommentSelect $commentSelect
     * @param string                              $join - метод соединения
     * @return $this
     */
    public function joinComment(CommentSelect $commentSelect = null, $join = self::JOIN_LEFT)
    {

        if ($commentSelect == null) {
            $commentSelect = new ProductCommentSelect();
        }

        $commentSelect
            ->columns(array(
                'product_comment_id' => 'id',
                'product_comment_user_id' => 'user_id',
                'product_comment_comment' => 'comment',
                'product_comment_date_public' => 'date_public',
                'product_comment_positive_votes' => 'positive_votes',
                'product_comment_negative_votes' => 'negative_votes',
                'product_comment_user_name_copy' => 'user_name_copy',
                'product_comment_user_lname_copy' => 'user_lname_copy',
            ));

        $this->joinFromSelect($commentSelect,
            $commentSelect->getTableName(). '.id=' . $this->getTableName() . '.comment_id',
            $join);

        return $this;
    }
}