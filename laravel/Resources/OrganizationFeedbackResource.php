<?php

namespace App\Http\Resources\Organization;

use App\Http\Resources\UserListResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin \App\Models\OrganizationFeedback
 */
class OrganizationFeedbackResource extends Resource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     * @throws \App\Exceptions\Pagination\PaginationException
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'author' => new UserListResource($this->author),
            'createdAt' => $this->created_at->toDateTimeString(),
        ];
    }
}
