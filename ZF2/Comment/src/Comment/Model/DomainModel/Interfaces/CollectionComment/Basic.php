<?php
namespace Comment\Model\DomainModel\Interfaces\CollectionComment;


interface Basic
{
    /**
     * Установить поле id
     * @param $id
     * @return $this
     */
    public function setId($id);

    /**
     * Получить поле id
     * @return integer
     */
    public function getId();

    /**
     * Установить поле collection_id
     * @param $collectionId
     * @return $this
     */
    public function setCollectionId($collectionId);

    /**
     * Получить поле collection_id
     * @return integer
     */
    public function getCollectionId();

    /**
     * Установить поле user_id
     * @param $userId
     * @return $this
     */
    public function setUserId($userId);

    /**
     * Получить поле user_id
     * @return integer
     */
    public function getUserId();

    /**
     * Установить поле comment
     * @param $comment
     * @return $this
     */
    public function setComment($comment);

    /**
     * Получить поле comment
     * @return string
     */
    public function getComment();

    /**
     * Установить поле date_public
     * @param $datePublic
     * @return $this
     */
    public function setDatePublic($datePublic);

    /**
     * Получить поле date_public
     * @return mixed
     */
    public function getDatePublic($mask);

    /**
     * Установить поле positive_votes
     * @param $positiveVotes
     * @return $this
     */
    public function setPositiveVotes($positiveVotes);

    /**
     * Получить поле positive_votes
     * @return integer
     */
    public function getPositiveVotes();

    /**
     * Установить поле negative_votes
     * @param $negativeVotes
     * @return $this
     */
    public function setNegativeVotes($negativeVotes);

    /**
     * Получить поле negative_votes
     * @return integer
     */
    public function getNegativeVotes();

}