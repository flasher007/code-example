<?php
namespace Comment\Model\DomainModel;

use Basic\Model\DomainModel;

abstract class CommentVote extends DomainModel
{
    /**
     * Установить поле id
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->setFieldValue('id', $id);
        return $this;
    }

    /**
     * Получить поле id
     * @return integer
     */
    public function getId()
    {
        return $this->getFieldValue('id');
    }

    /**
     * Установить поле ссылку на коментарий
     * @param $commentId
     * @return $this
     */
    abstract public function setCommentId($commentId);

    /**
     * Получить поле product_comment_id
     * @return integer
     */
    abstract public function getCommentId();

    /**
     * Установить поле user_id
     * @param $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->setFieldValue('user_id', $userId);
        return $this;
    }

    /**
     * Получить поле user_id
     * @return integer
     */
    public function getUserId()
    {
        return $this->getFieldValue('user_id');
    }

    /**
     * Установить поле is_positive
     * @param $isPositive
     * @return $this
     */
    public function setIsPositive($isPositive)
    {
        $this->setFieldValue('is_positive', $isPositive);
        return $this;
    }

    /**
     * Получить поле is_positive
     * @return boolean
     */
    public function getIsPositive()
    {
        return $this->getFieldValue('is_positive');
    }


    protected function setFields()
    {

        $this->setIntField('id', null, true);
        $this->setIntField('user_id', null, true);
        $this->setBoolField('is_positive', false, true);
    }

    protected function setRelatedEntities()
    {
    }
}