<?php
namespace Comment\Model\Select;

use Basic\Db\Select;

class CollectionVoteSelect extends VoteSelect
{
    /**
     * @var string|\Zend\Db\Sql\TableIdentifier
     */
    protected $table = 'collection_vote';

    /**
     * Создает условие по запросу
     * @param \Comment\Model\Request\VoteListRequest $request
     * @return $this
     */
    public function formConditionByRequest($request)
    {
        parent::formConditionByRequest($request);

        return $this;
    }

    /**
     * добавляет к выборке условие на поле collection_id
     * @param int|array $entityId - один id продукта или массив айдишников продуктов
     * @return $this
     */
    public function whereEntityId($entityId)
    {
        $table = $this->getTableName();
        $this->where(array($table . '.collection_id' => $entityId));

        return $this;
    }

    /**
     * Join таблицы комментариев
     * @param \Comment\Model\Select\CommentSelect $commentSelect
     * @param string                              $join - метод соединения
     * @return $this
     */
    public function joinComment(CommentSelect $commentSelect = null, $join = self::JOIN_LEFT)
    {

        if ($commentSelect == null) {
            $commentSelect = new CollectionCommentSelect();
        }

        $commentSelect
            ->columns(array(
                'collection_comment_id' => 'id',
                'collection_comment_user_id' => 'user_id',
                'collection_comment_comment' => 'comment',
                'collection_comment_date_public' => 'date_public',
                'collection_comment_positive_votes' => 'positive_votes',
                'collection_comment_negative_votes' => 'negative_votes',
                'collection_comment_user_name_copy' => 'user_name_copy',
                'collection_comment_user_lname_copy' => 'user_lname_copy',
            ));

        $this->joinFromSelect($commentSelect,
            $commentSelect->getTableName(). '.id=' . $this->getTableName() . '.comment_id',
            $join);

        return $this;
    }
}