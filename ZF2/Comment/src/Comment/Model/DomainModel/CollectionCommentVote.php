<?php
namespace Comment\Model\DomainModel;

use Basic\Model\DomainModel;
use Comment\Model\DomainModel\Interfaces\CollectionComment\Basic as CollectionCommentBasic;
use User\Model\User as UserBasic;

class CollectionCommentVote extends CommentVote
{
    /**
     * Установить поле ссылку на коментарий
     * @param $commentId
     * @return $this
     */
    public function setCommentId($commentId)
    {
        return $this->setCollectionCommentId($commentId);
    }

    /**
     * Получить поле collection_comment_id
     * @return integer
     */
    public function getCommentId()
    {
        return $this->getCollectionCommentId();
    }

    /**
     * Установить поле collection_comment_id
     * @param $collectionCommentId
     * @return $this
     */
    public function setCollectionCommentId($collectionCommentId)
    {
        $this->setFieldValue('collection_comment_id', $collectionCommentId);
        return $this;
    }

    /**
     * Получить поле collection_comment_id
     * @return integer
     */
    public function getCollectionCommentId()
    {
        return $this->getFieldValue('collection_comment_id');
    }


    /**
     * Установить связанную сущность \Comment\Model\DomainModel\CollectionComment
     * @param CollectionCommentBasic $collectionComment
     * @return $this
     */
    public function setCollectionComment(CollectionCommentBasic $collectionComment)
    {
        $this->setFieldValue('collection_comment', $collectionComment);
        return $this;
    }

    /**
     * Получить поле collection_comment
     * @return \Comment\Model\DomainModel\CollectionComment
     */
    public function getCollectionComment()
    {
        return $this->getFieldValue('collection_comment');
    }

    /**
     * Установить связанную сущность \User\Model\User
     * @param UserBasic $user
     * @return $this
     */
    public function setUser(UserBasic $user)
    {
        $this->setFieldValue('user', $user);
        return $this;
    }

    /**
     * Получить поле user
     * @return \User\Model\User
     */
    public function getUser()
    {
        return $this->getFieldValue('user');
    }

    protected function setFields()
    {
        parent::setFields();
        $this->setIntField('collection_comment_id', null, true);
    }

    protected function setRelatedEntities()
    {
        $this->setRelatedEntity('collection_comment', '\Comment\Model\DomainModel\CollectionComment');
        $this->setRelatedEntity('user', '\User\Model\User');
    }
}