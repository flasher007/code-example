<?php
namespace Comment\Model\DomainModel\Interfaces\ProductCommentVote;

use User\Model\User as UserBasic;


interface User extends Basic
{
    /**
     * Установить связанную сущность \User\Model\User
     * @param UserBasic $user
     * @return $this
     */
    public function setUser(UserBasic $user);

    /**
     * Получить поле user
     * @return \User\Model\User
     */
    public function getUser();

}