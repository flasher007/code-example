<?php
namespace Comment\Model\DomainModel\Interfaces\ProductComment;

use Shop\Model\DomainModel\Interfaces\Product\Basic as ProductBasic;

/**
 * Class Category
 * @package Shop\Model\DomainModel\Interfaces\Product
 */
interface Product extends Basic
{
    /**
     * Установить связанную сущность \Shop\Model\DomainModel\Product
     * @param ProductBasic $product
     * @return $this
     */
    public function setProduct(ProductBasic $product);

    /**
     * Получить поле product
     * @return \Shop\Model\DomainModel\Product
     */
    public function getProduct();

}