<?php

namespace App\Http\Requests\Organization;

use App\Services\Organization\OrganizationService;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationDestroyEmployeeRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(OrganizationService $service): bool
    {
        $org = $service->findOrFail($this->route('id'));
        return $this->user()->can('delete-employee', $org);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [];
    }

}
