<?php
namespace Comment\Model\Table;


/**
 * @method \Comment\Model\Select\ProductCommentSelect getSelect()
 */
class ProductCommentTable extends CommentTable
{
    protected $table = 'product_comment';
    protected $resultObjectPrototype = '\Comment\Model\DomainModel\ProductComment';
    protected $selectClassName = '\Comment\Model\Select\ProductCommentSelect';

    /**
     * Удаляет в корзину записи, из связанных таблиц. В каждом потомке необходимо прописывать код удаления
     * @param int $id              - ид удалямой записи
     * @param int $ownerId         - владелец, удаливший запись
     * @param int $operationNumber - номер операции. Если null - выберется автоматом новый номер
     * @return bool - успех операции
     */
    protected function deleteRelatedEntities($id, $ownerId = null, $operationNumber = null)
    {
        $table = new ProductCommentVoteTable();
        $items = $table->fetchAllOfComments($id);
        $res = $this->deleteItemsOfRelatedEntity($table, $items, $ownerId, $operationNumber);
        if (!$res) {
            return false;
        }

        return true;
    }
}