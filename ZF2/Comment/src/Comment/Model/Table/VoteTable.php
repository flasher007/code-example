<?php
namespace Comment\Model\Table;

use Basic\Db\Table;
use Comment\Model\Select\ProductCommentSelect;
use User\Model\Select\UserSelect;

/**
 * @method \Comment\Model\Select\VoteSelect getSelect()
 */
abstract class VoteTable extends Table
{
    protected $table = 'product_vote';
    protected $resultObjectPrototype = '\Comment\Model\DomainModel\Vote';
    protected $selectClassName = '\Comment\Model\Select\VoteSelect';

    /**
     * @return \Comment\Model\Select\CommentSelect
     */
    abstract public function getCommentSelect();


    /**
     * Возвращает все записи, отсортированные по имени
     * @param \Comment\Model\Request\VoteListRequest $request - запрос на выборку
     * @return  \Comment\Model\DomainModel\Interfaces\ProductVote\UserProductComment[]|\Basic\Db\ResultSet
     */
    public function fetchAllWithCommentByRequest(\Comment\Model\Request\VoteListRequest $request)
    {
        $select = $this->getSelect();
        $select->formConditionByRequest($request);
        $pcSelect = $this->getCommentSelect();
        $userSelect = new UserSelect();
        $userSelect->whereStatusActive();
        $pcSelect->joinUser($userSelect);
        $pcSelect->orderByDate(false);

        $select->joinComment($pcSelect, ProductCommentSelect::JOIN_INNER);

        return $this->fetchAll();
    }

    /**
     * Возвращает все голоса за продукт
     * @param int    $entityId
     * @return \Comment\Model\DomainModel\Interfaces\Vote\Basic[]
     */
    public function fetchAllOfEntity($entityId)
    {
        $select = $this->getSelect();
        $select->whereEntityId($entityId);

        return $this->fetchAll();
    }

    /**
     * Возвращает все записи, отсортированные по имени
     * @param string $email
     * @param int    $entityId
     * @return \Shop\Model\DomainModel\Interfaces\ProductVote\Basic[]
     */
    public function fetchAllOfEntityByEmail($email, $entityId)
    {
        $select = $this->getSelect();
        $select->whereEmail($email);

        return $this->fetchAllOfEntity($entityId);
    }

    /**
     * Возвращает все записи, отсортированные по имени
     * @param string $userId
     * @param int    $entityId
     * @return \Shop\Model\DomainModel\Interfaces\ProductVote\Basic[]
     */
    public function fetchAllOfEntityByUserId($userId, $entityId)
    {
        $select = $this->getSelect();
        $select->whereUserId($userId);

        return $this->fetchAllOfEntity($entityId);
    }

    /**
     * Возвращает все записи, отсортированные по имени
     * @param string $userId
     * @return \Shop\Model\DomainModel\Interfaces\ProductVote\Basic[]
     */
    public function fetchAllByUserId($userId)
    {
        $select = $this->getSelect();
        $select->whereUserId($userId);

        return $this->fetchAll();
    }

    /**
     * Возвращает все записи, отсортированные по имени
     * @param string $userId
     * @param int    $entityId
     * @return \Shop\Model\DomainModel\Interfaces\ProductVote\Basic|null
     */
    public function fetchUserVoteToEntity($userId, $entityId)
    {
        $select = $this->getSelect();
        $select->whereEntityId($entityId);
        $select->whereUserId($userId);
        $select->limit(1);

        return $this->fetchOne();
    }
}