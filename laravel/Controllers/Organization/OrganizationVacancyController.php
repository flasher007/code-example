<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Resources\Vacancy\VacancyFullResource;
use App\Services\Organization\OrganizationService;

class OrganizationVacancyController extends Controller
{
    // GET /organizations/{id}/vacancies
    public function index(int $id, OrganizationService $service)
    {
        return VacancyFullResource::collection($service->vacancies($id));
    }
}
