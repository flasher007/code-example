<?php
namespace Comment;

class Module
{
    public function onBootstrap($e){
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'comment.product_comment_service' => function($serviceManager) {
                    $service = new \Comment\Service\ProductCommentService();
                    $service->setServiceLocator($serviceManager);

                    return $service;
                },
                'comment.collection_comment_service' => function($serviceManager) {
                    $service = new \Comment\Service\CollectionCommentService();
                    $service->setServiceLocator($serviceManager);

                    return $service;
                },
            )
        );
    }


}