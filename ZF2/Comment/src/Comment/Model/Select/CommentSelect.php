<?php
namespace Comment\Model\Select;

use Basic\Db\Select;
use User\Model\Select\UserSelect;

abstract class CommentSelect extends Select
{
    /**
     * @var string|\Zend\Db\Sql\TableIdentifier
     */
    protected $table = 'comment';

    /**
     * Задает сортировку по дате
     * @param boolean $asc - если правда - сортировка идет на возрастание
     * @return $this
     */
    public function orderByDate($asc)
    {
        if ($asc) {
            $this->order(array('date_public' => 'ASC'));
        } else {
            $this->order(array('date_public' => 'DESC'));
        }

        return $this;
    }


    /**
     * добавляет к выборке условие на ключ к таблице, к которой относятся комментарии
     * @param int|array $entityId - айдиха сущности к которой относятся комментарии
     * @return $this
     */
    abstract public function whereEntityId($entityId);


    /**
     * добавляет к выборке условие на поле user_id
     * @param int|array $userId - айдиха пользователя
     * @return $this
     */
    public function whereUserId($userId)
    {
        $table = $this->getTableName();
        $this->where(array($table . '.user_id' => $userId));

        return $this;
    }

    /**
     * Join таблицы продуктов
     * @param \User\Model\Select\UserSelect $userSelect
     * @param string $join - метод соединения
     * @return $this
     */
    public function joinUser(UserSelect $userSelect = null, $join = self::JOIN_LEFT)
    {
        if ($userSelect == null) {
            $userSelect = new UserSelect();
        }

        $userSelect
            ->columns(array(
                'user_name' => 'name',
                'user_lname' => 'lname',
            ));

        $this->joinFromSelect($userSelect,
            $userSelect->getTableName(). '.id=' . $this->getTableName() . '.user_id',
            $join);

        return $this;
    }

}