<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\Organization\OrganizationAdminDestroyRequest;
use App\Http\Requests\Organization\OrganizationAdminStoreRequest;
use App\Services\Organization\OrganizationService;

class OrganizationAdminController extends Controller
{
    // DELETE /organizations/{id}/admins/{userId}
    public function destroy(int $id, int $userId, OrganizationAdminDestroyRequest $request, OrganizationService $service)
    {
        $service->detachAdmin($id, $userId);

        return response('', 204);
    }

    // POST /organizations/{id}/admins
    public function store(int $id, OrganizationAdminStoreRequest $request, OrganizationService $service)
    {
        $service->attachAdmin($id, $request->input('userId'));

        return response('', 201);
    }
}
