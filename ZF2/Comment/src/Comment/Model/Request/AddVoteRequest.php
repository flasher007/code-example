<?php
namespace Comment\Model\Request;

use Basic\Model\DomainModel;
use Shop\Model\DomainModel\Interfaces\Brand as BrandInterface;
use Zend\Validator\GreaterThan;
use Zend\Validator\LessThan;


/**
 * Доменная модель продукта
 * Class Entity
 * @package Shop\Model\DomainModel
 */
class AddVoteRequest extends DomainModel
{


    /**
     * Установить имя продукта
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->setFieldValue('name', $name);
        return $this;
    }

    public function getName()
    {
        return $this->getFieldValue('name');
    }

    /**
     * Установить фамилию
     * @param $name
     * @return $this
     */
    public function setLName($name)
    {
        $this->setFieldValue('lname', $name);
        return $this;
    }

    public function getLName()
    {
        return $this->getFieldValue('lname');
    }

    /**
     *
     * @param $estimate
     * @return $this
     */
    public function setEstimate($estimate)
    {
        $this->setFieldValue('estimate', $estimate);
        return $this;
    }

    public function getEstimate()
    {
        return $this->getFieldValue('estimate');
    }

    /**
     *
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->setFieldValue('email', $email);
        return $this;
    }

    public function getEmail()
    {
        return $this->getFieldValue('email');
    }

    /**
     *
     * @param $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->setFieldValue('comment', $comment);
        return $this;
    }

    public function getComment()
    {
        return $this->getFieldValue('comment');
    }

    /**
     *
     * @param $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->setFieldValue('entity_id', $entityId);
        return $this;
    }

    public function getEntityId()
    {
        return $this->getFieldValue('entity_id');
    }

    /**
     *
     * @param $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->setFieldValue('user_id', $userId);
        return $this;
    }

    public function getUserId()
    {
        return $this->getFieldValue('user_id');
    }

    public function removeEmail()
    {
        $this->removeFilters('email');
        $this->removeValidators('email');
        $this->clearField('email');
        $this->removeRequired('email');
    }

    public function removeCaptcha()
    {
        $this->removeFilters('captcha');
        $this->removeValidators('captcha');
        $this->clearField('captcha');
        $this->removeRequired('captcha');
    }

    public function removeUserName()
    {
        $this->removeFilters('name');
        $this->removeValidators('name');
        $this->clearField('name');
        $this->removeRequired('name');
    }

    public function removeUserLName()
    {
        $this->removeFilters('lname');
        $this->removeValidators('lname');
        $this->clearField('lname');
        $this->removeRequired('lname');
    }

    public function removeUserNames()
    {
        $this->removeFilters('name');
        $this->removeValidators('name');
        $this->removeFilters('lname');
        $this->removeValidators('lname');
        $this->clearField('name');
        $this->clearField('lname');
        $this->removeRequired('name');
        $this->removeRequired('lname');
    }

    protected function setFields()
    {
        $this->setStringField('name', null, true, true, 0, 50);
        $this->setStringField('lname', null, true, true, 0, 50);
        $this->setStringField('email', null, true, true, 0, 255);
        $this->addValidator('email', array(
            'name' => 'EmailAddress',
        ));
        $this->setIntField('estimate', null, true, 1, 6);
        $this->setMessageToValidator('estimate', self::VALIDATOR_GREATER_OR_EQUAL, array(
            GreaterThan::NOT_GREATER_INCLUSIVE => 'Оценка должна быть от 1 до 5'
        ));
        $this->setMessageToValidator('estimate', self::VALIDATOR_LESS_THEN, array(
            LessThan::NOT_LESS => 'Оценка должна быть от 1 до 5'
        ));
        $this->setStringField('comment', null, false, true, 0, 500);
        $this->setIntField('user_id', null, false);
        $this->setIntField('entity_id');
        $this->setField('captcha', self::FIELD_TYPE_RAW);
    }

    protected function setRelatedEntities()
    {

    }
}