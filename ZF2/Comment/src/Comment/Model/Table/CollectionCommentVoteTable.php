<?php
namespace Comment\Model\Table;


/**
 * @method \Comment\Model\Select\CollectionCommentVoteSelect getSelect()
 */
class CollectionCommentVoteTable extends CommentVoteTable
{
    protected $table = 'collection_comment_vote';
    protected $resultObjectPrototype = '\Comment\Model\DomainModel\CollectionCommentVote';
    protected $selectClassName = '\Comment\Model\Select\CollectionCommentVoteSelect';
}