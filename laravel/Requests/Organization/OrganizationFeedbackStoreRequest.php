<?php

namespace App\Http\Requests\Organization;

use App\Http\Requests\FormRequest;

class OrganizationFeedbackStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'text' => 'required|string|between:5,1000',
            'organizationId' => 'required|integer|min:1|exists:organizations,id',
        ];
    }

    public function sanitize(array $data): array
    {
        $data['organizationId'] = $this->route('id');

        return $data;
    }
}
