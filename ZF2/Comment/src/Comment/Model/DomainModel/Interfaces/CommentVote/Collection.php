<?php
namespace Comment\Model\DomainModel\Interfaces\CollectionVote;

use User\Model\Collection as CollectionBasic;


interface Collection extends Basic
{
    /**
     * Установить связанную сущность \User\Model\Collection
     * @param CollectionBasic $collection
     * @return $this
     */
    public function setCollection(CollectionBasic $collection);

    /**
     * Получить поле collection
     * @return \User\Model\Collection
     */
    public function getCollection();

}