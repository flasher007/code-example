<?php
namespace Comment\Model\DomainModel\Interfaces\CollectionComment;


use User\Model\Collection as CollectionBasic;


interface Collection extends Basic
{
    /**
     * Установить связанную сущность \User\Model\Collection
     * @param \User\Model\Collection $collection
     * @return $this
     */
    public function setCollection(CollectionBasic $collection);

    /**
     * Получить поле collection
     * @return \User\Model\Collection
     */
    public function getCollection();

}