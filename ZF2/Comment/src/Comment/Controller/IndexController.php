<?php

namespace Comment\Controller;


use Basic\Controller\FrontendController;
use Comment\Model\Request\AddVoteRequest;
use Comment\Model\Request\VoteListRequest;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Базовый контроллер для всех комментариев в системе
*/
abstract class IndexController extends FrontendController
{

    /**
     * Возвращает сервис комментариев
     * @return \Comment\Service\CommentService
     */
    abstract protected function getCommentService();

    /**
     * Возвращает имя шаблона
     * @return string
     */
    abstract protected function getListTemplate();

    /**
     * Возвращает имя шаблона
     * @return string
     */
    abstract protected function getFormTemplate();

    /**
     * Возвращает список комментариев к продукту
     * @return \Zend\View\Model\ViewModel
     */
    public function commentListAction()
    {
        $commentService = $this->getCommentService();
        $user = null;
        $userId = 0;
        //Если пользовтель авторизован
        if ($this->zfcUserAuthentication()->hasIdentity()) {
            /**
             * @var \ZfcUser\Entity\User $user
             */
            $user = $this->getServiceLocator()->get('zfcuser_user_service')->getAuthService()->getIdentity();
            $userId = $user->getId();
        }
        $request = $this->getRequest();

        $reqData = $request->getQuery()->toArray();
        if (isset($reqData['page']) && $reqData['page'] == 'all') {
            $reqData['page'] = 1;
            $reqData['count_per_page'] = 999;
        }
        $voteListRequest = new VoteListRequest();
        $voteListRequest->exchangeArray($reqData);

        //хак для оптимизации, пока нет пагинатора. Как появится - выбираем продукт и смотрим сколько у него комментов
        $votes = $commentService->getVotesByListRequest($voteListRequest, 999);
        $userVotes = array();
        $doVoteAble = false;
        if ($user) {
            $userVotes = $commentService->getUserVotesToProductVotes($user->getId(), $votes)
                ->copyAsIdValueMap('product_comment_id', 'is_positive');
            $doVoteAble = true;
        }

        $viewModel = new ViewModel(array(
            'votes'      => $votes,
            'currentUserId' => $userId,
            'userVotes'  => $userVotes,
            'doVoteAble' => $doVoteAble,
            'deleteAble' => $this->isAllowed('edit-product-comments','edit'),
            'pathToAction' => $this->getPathToAction()
        ));
        $viewModel->setTemplate($this->getListTemplate());
        $viewModel->setTerminal(true);

        return $viewModel;
    }

    /**
     * Возвращает имя контроллера в пути к странице
     * @return string
     */
    abstract public function getPathToAction();

    public function commentFormAction()
    {
        $user = null;
        //Если пользовтель авторизован
        if ($this->zfcUserAuthentication()->hasIdentity()) {
            /**
             * @var \ZfcUser\Entity\User $user
             */
            $user = $this->getServiceLocator()->get('zfcuser_user_service')->getAuthService()->getIdentity();
        }
        $request        = $this->getRequest();
        $entityId      = $this->params()->fromRoute('id');
        $commentService = $this->getCommentService();
        $form = $commentService->getCommentForm($entityId, $user);
        if ($form) {
            if ($request->isPost()) {
                $data = $request->getPost()->toArray();
                $data['entity_id'] = $entityId;
                $addVoteRequest = new AddVoteRequest();
                $addVoteRequest->exchangeArray($data);
                $success = $commentService->saveDataOfCommentForm($addVoteRequest, $form, $user );
                //Если ошибок не было - редиректим
                if ($success) {
                    return true;
                } else {
                    $msg = $form->getMessages();
                    $form->setData($data);
                    $form->isValid();
                }
            }
            $form->prepare();
        }

        $viewModel = new ViewModel(array(
            'commentForm' => $form,
        ));
        $viewModel->setTemplate($this->getFormTemplate());
//        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * Изменяет рейтинг у комментария
     * @return \Zend\View\Model\JsonModel
     */
    public function changeRatingAction()
    {
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            return new JsonModel(array(
                'success' => false,
                'message' => 'Данный функционал доступен только авторизированным пользователям',
                'code'    => 1
            ));
        }

        $commentService = $this->getCommentService();

        $commentId = (int)$this->params()->fromRoute('comment_id', 0);
        $isPositive   = (boolean)$this->params()->fromQuery('is_positive', 0);
        $userId       = $this->getServiceLocator()
            ->get('zfcuser_user_service')
            ->getAuthService()
            ->getIdentity()
            ->getId();
        $vote    = $commentService->addVoteToComment($userId, $commentId, $isPositive);
        $comment = $commentService->getComment($commentId);
        $positiveRating       = 0;
        $negativeRating       = 0;

        if ($comment) {
            $positiveRating = $comment->getPositiveVotes();
            $negativeRating = $comment->getNegativeVotes();
        }

        if ($vote) {
            return new JsonModel(array(
                'success' => true,
                'data'    => array(
                    'vote'   => $vote->getArrayCopy(),
                    'positiveRating' => $positiveRating,
                    'negativeRating' => $negativeRating
                )
            ));
        } else {
            return new JsonModel(array(
                'success' => false,
                'message' => 'Вы уже голосовали за данный комментарий',
                'code'    => 2
            ));
        }
    }

    public function deleteCommentAction()
    {
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            return new JsonModel(array(
                'success' => false,
                'message' => 'Данный функционал доступен только авторизированным пользователям',
                'code'    => 1
            ));
        }
        if (!$this->isAllowed('edit-product-comments','edit')) {
            return new JsonModel(array(
                'success' => false,
                'message' => 'У вас нет прав на удаление комментария',
                'code'    => 3
            ));
        }

        //определяем добавляемый продукт
        $voteId = $this->params()->fromQuery('vote_id', 0);

        /**
         * @var \Shop\Service\ProductService $productService
         */
        $commentService = $this->getCommentService();
        $vote = $commentService->getVoteById($voteId);

        if (!$vote) {
            return new JsonModel(array(
                'success' => false,
                'message' => 'Запрошенный вами комментарий не был найден',
                'code'    => 2
            ));
        }

        $user = $this->getServiceLocator()->get('zfcuser_user_service')->getAuthService()->getIdentity();
        $success = $commentService->deleteVote($voteId, $user->getId());

        if ($success) {
            return new JsonModel(array(
                'success' => true,
                'message' => 'Комментарий был удачно удален'
            ));
        } else {
            return new JsonModel(array(
                'success' => false,
                'message' => 'Не удалось удалить комментарий',
                'code'    => 4
            ));
        }

    }

    public function saveCommentAction()
    {
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            return new JsonModel(array(
                'success' => false,
                'message' => 'Данный функционал доступен только авторизированным пользователям',
                'code'    => 1
            ));
        }
        if (!$this->isAllowed('edit-product-comments','edit')) {
            return new JsonModel(array(
                'success' => false,
                'message' => 'У вас нет прав на удаление комментария',
                'code'    => 3
            ));
        }

        //определяем сохраняемый комментарий
        $commentId = $this->params()->fromQuery('comment_id', 0);
        $commentText = $this->params()->fromQuery('comment', '');

        $commentService = $this->getCommentService();
        $comment = $commentService->getComment($commentId);

        if (!$comment) {
            return new JsonModel(array(
                'success' => false,
                'message' => 'Запрошенный вами комментарий не был найден',
                'code'    => 2
            ));
        }

        $success = $commentService->saveComment($commentText, $commentId);

        if ($success) {
            return new JsonModel(array(
                'success' => true,
                'message' => 'Комментарий был удачно отредактирован'
            ));
        } else {
            return new JsonModel(array(
                'success' => false,
                'message' => 'Не удалось изменить комментарий',
                'code'    => 4
            ));
        }

    }
}
