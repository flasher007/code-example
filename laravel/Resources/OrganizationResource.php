<?php

namespace App\Http\Resources\Organization;

use App\Http\Resources\Vacancy\VacancyLightResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin \App\Models\Organization
 */
class OrganizationResource extends Resource
{
    public function toArray($request): array
    {
        /** @var \App\Models\User $user */
        $user = $request->user();

        return [
            'id' => $this->id,
            'inn' => $this->inn,
            'kpp' => $this->kpp,
            'types' => $this->types,
            'email' => $this->email,
            'title' => $this->title,
            'website' => $this->website,
            'personName' => $this->person_name,
            'shortTitle' => $this->short_title,
            'description' => $this->description,
            'personEmail' => $this->person_email,
            'personPhone' => $this->person_phone,
            'addressPhysical' => $this->address_physical,
            'legalEntityType' => $this->legal_entity_type,
            'addressRegistered' => $this->address_registered,

            'isAdmin' => $this->when(null !== $user, function () use ($user) {
                return $this->isAdmin($user->id);
            }, false),

            'vacanciesCount' => $this->vacancies_count,
            'employeesCount' => $this->employees_count,
            'projectsCount' => $this->projects_count,
            'lastVacancies' => VacancyLightResource::collection($this->lastVacancies)
        ];
    }
}
