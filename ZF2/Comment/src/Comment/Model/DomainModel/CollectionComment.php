<?php
namespace Comment\Model\DomainModel;

use Basic\Model\DomainModel;
use User\Model\Collection as CollectionBasic;
use User\Model\User as UserBasic;

class CollectionComment extends DomainModel implements
    Interfaces\CollectionComment\Basic,
    Interfaces\CollectionComment\User,
    Interfaces\CollectionComment\Collection
{
    /**
     * Установить поле id
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->setFieldValue('id', $id);
        return $this;
    }

    /**
     * Получить поле id
     * @return integer
     */
    public function getId()
    {
        return $this->getFieldValue('id');
    }

    /**
     * Установить поле collection_id
     * @param $collectionId
     * @return $this
     */
    public function setCollectionId($collectionId)
    {
        $this->setFieldValue('collection_id', $collectionId);
        return $this;
    }

    /**
     * Получить поле collection_id
     * @return integer
     */
    public function getCollectionId()
    {
        return $this->getFieldValue('collection_id');
    }

    /**
     * Установить поле user_id
     * @param $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->setFieldValue('user_id', $userId);
        return $this;
    }

    /**
     * Получить поле user_id
     * @return integer
     */
    public function getUserId()
    {
        return $this->getFieldValue('user_id');
    }

    /**
     * Установить поле comment
     * @param $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->setFieldValue('comment', $comment);
        return $this;
    }

    /**
     * Получить поле comment
     * @return string
     */
    public function getComment()
    {
        return $this->getFieldValue('comment');
    }

    /**
     * Установить поле date_public
     * @param $datePublic
     * @return $this
     */
    public function setDatePublic($datePublic)
    {
        $this->setFieldValue('date_public', $datePublic);
        return $this;
    }

    /**
     * Получить поле date_public
     * @return mixed
     */
    public function getDatePublic($mask)
    {
        $date = $this->getFieldValue('date_public');
        $date = date($mask, strtotime($date));
        return $date;
    }

    /**
     * Установить поле positive_votes
     * @param $positiveVotes
     * @return $this
     */
    public function setPositiveVotes($positiveVotes)
    {
        $this->setFieldValue('positive_votes', $positiveVotes);
        return $this;
    }

    /**
     * Получить поле positive_votes
     * @return integer
     */
    public function getPositiveVotes()
    {
        return $this->getFieldValue('positive_votes');
    }

    /**
     * Установить поле negative_votes
     * @param $negativeVotes
     * @return $this
     */
    public function setNegativeVotes($negativeVotes)
    {
        $this->setFieldValue('negative_votes', $negativeVotes);
        return $this;
    }

    /**
     * Получить поле negative_votes
     * @return integer
     */
    public function getNegativeVotes()
    {
        return $this->getFieldValue('negative_votes');
    }

    /**
     * Установить поле user_name
     * @param $userName
     * @return $this
     */
    public function setUserName($userName)
    {
        $this->setFieldValue('user_name_copy', $userName);
        return $this;
    }

    /**
     * Получить поле user_name
     * @return string
     */
    public function getUserName()
    {
        $user = $this->getUser();
        $userName = $user->name;
        if ($userName) {
            return $userName;
        }
        return $this->getFieldValue('user_name_copy');
    }

    /**
     * Установить поле user_lname
     * @param $userLname
     * @return $this
     */
    public function setUserLname($userLname)
    {
        $this->setFieldValue('user_lname_copy', $userLname);
        return $this;
    }

    /**
     * Получить поле user_lname
     * @return string
     */
    public function getUserLname()
    {
        $user = $this->getUser();
        $userName = $user->lname;
        if ($userName) {
            return $userName;
        }
        return $this->getFieldValue('user_lname_copy');
    }


    /**
     * Установить связанную сущность \Shop\Model\DomainModel\Collection
     * @param CollectionBasic $collection
     * @return $this
     */
    public function setCollection(CollectionBasic $collection)
    {
        $this->setFieldValue('collection', $collection);
        return $this;
    }

    /**
     * Получить поле collection
     * @return \Shop\Model\DomainModel\Collection
     */
    public function getCollection()
    {
        return $this->getFieldValue('collection');
    }

    /**
     * Установить связанную сущность \Shop\Model\DomainModel\User
     * @param UserBasic $user
     * @return $this
     */
    public function setUser(UserBasic $user)
    {
        $this->setFieldValue('user', $user);
        return $this;
    }

    /**
     * Получить поле user
     * @return \User\Model\User
     */
    public function getUser()
    {
        return $this->getFieldValue('user');
    }

    protected function setFields()
    {

        $this->setIntField('id', null, true);
        $this->setIntField('collection_id', null, true);
        $this->setIntField('user_id', null, true);
        $this->setStringField('comment', null, true, true, 1, 500);
        $this->setStringField('date_public', null, true, true, 1, 16);
        $this->setIntField('positive_votes', null, true);
        $this->setIntField('negative_votes', null, true);
        $this->setStringField('user_name_copy', null, false, true, 1, 50);
        $this->setStringField('user_lname_copy', null, false, true, 1, 50);
    }

    protected function setRelatedEntities()
    {
        $this->setRelatedEntity('collection', '\Shop\Model\DomainModel\Collection');
        $this->setRelatedEntity('user', '\User\Model\User');
    }

    public function exchangeArray($data)
    {
        $data = parent::exchangeArray($data);
        if (array_key_exists('entity_id', $data)) {
            $this->setFieldValue('collection_id', $data['entity_id']);
        }
        return $data;
    }
}