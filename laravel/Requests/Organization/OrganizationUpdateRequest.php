<?php

namespace App\Http\Requests\Organization;

use App\Models\Enums\OrganizationLegalEntityTypeEnum;
use App\Models\Enums\OrganizationTypeEnum;
use App\Rules\Inn;
use App\Rules\Phone;
use App\Services\Organization\OrganizationService;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationUpdateRequest extends FormRequest
{
    public function authorize(OrganizationService $service): bool
    {
        $org = $service->findOrFail($this->route('id'));

        return $this->user()->can('update', $org);
    }

    public function rules(): array
    {
        return [
            'inn' => ['required', 'string', Inn::REGEX],
            'kpp' => 'required|string|regex:/^\d{9}$/',
            'email' => 'required|email|max:255',
            'phone' => ['required', Phone::REGEX],
            'title' => 'required|string|max:255',
            'types' => ['required', 'array'],
            'types.*' => new EnumValue(OrganizationTypeEnum::class),
            'website' => 'nullable|string|url|max:255',
            'personName' => 'required|max:255',
            'shortTitle' => 'required|string|max:255',
            'description' => 'required|max:1000',
            'personPhone' => ['required', Phone::REGEX],
            'personEmail' => 'required|email|max:255',
            'addressPhysical' => 'required|string|max:255',
            'legalEntityType' => ['required', new EnumValue(OrganizationLegalEntityTypeEnum::class)],
            'addressRegistered' => 'required|string|max:255',
        ];
    }
}
