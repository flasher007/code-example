<?php

declare(strict_types=1);

/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace HypeCorp\Bundle\UserBundle\Controller;

use HypeCorp\Bundle\UserBundle\Form\LoginFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthController extends Controller
{
    /**
     * @param Request            $request
     * @param UserInterface|null $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request, UserInterface $user = null)
    {
        if (null !== $user) {
            return $this->redirectToRoute('dashboard');
        }

        $authenticationUtils = $this->get('security.authentication_utils');

        $form = $this->createForm(LoginFormType::class);
        $form->handleRequest($request);

        return $this->render('@User/user/auth/login.html.twig', [
            'last_username' => $authenticationUtils->getLastUsername(),
            'error' => $authenticationUtils->getLastAuthenticationError(),
            'form' => $form->createView(),
        ]);
    }
}
