<?php
namespace Comment\Model\Select;

use Basic\Db\Select;

class ProductCommentVoteSelect extends CommentVoteSelect
{
    /**
     * @var string|\Zend\Db\Sql\TableIdentifier
     */
    protected $table = 'product_comment_vote';

    /**
     * Добавляет условие на отсеивание по полю комментария
     * @param int|array $commentId - айдиха или несколько айдих комментариев
     * @return $this
     */
    public function whereCommentId($commentId)
    {
        $table = $this->getTableName();
        $this->where(array($table . '.' . 'product_comment_id' => $commentId));
        return $this;
    }
}