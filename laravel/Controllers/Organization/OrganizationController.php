<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\Organization\OrganizationAddRequest;
use App\Http\Requests\Organization\OrganizationUpdateRequest;
use App\Http\Resources\Organization\OrganizationAddRequestResource;
use App\Http\Resources\Organization\OrganizationResource;
use App\Services\Organization\OrganizationService;

class OrganizationController extends Controller
{
    /** @var OrganizationService */
    protected $service;

    public function __construct(OrganizationService $service)
    {
        $this->service = $service;
    }

    // GET /organizations/{id}
    public function show(int $id)
    {
        $org = $this->service->show($id);

        return new OrganizationResource($org);
    }

    // PUT /organizations/{id}
    public function update(int $id, OrganizationUpdateRequest $request)
    {
        $data = convertKeysToSnakeCase($request->validated());
        $org = $this->service->update($id, $data);

        return new OrganizationResource($org);
    }

    /**
     * POST /organizations/add
     *
     * @param OrganizationAddRequest $request
     * @return OrganizationAddRequestResource
     * @throws \App\Exceptions\Organization\AddRequestThrottleException
     */
    public function add(OrganizationAddRequest $request)
    {
        $addRequest = $this->service->storeAddRequest(convertKeysToSnakeCase($request->validated()));
        return new OrganizationAddRequestResource($addRequest);
    }
}
