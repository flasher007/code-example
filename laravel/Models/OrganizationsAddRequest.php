<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property string $title
 * @property bool $is_legal_person
 * @property string $feedback_username
 * @property string $phone
 * @property string $email
 * @property int $user_id
 * @property \Illuminate\Support\Carbon $processed_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property User $author
 */
class OrganizationsAddRequest extends BaseModel
{
    /**
     * @var array
     */
    protected $casts = [
        'is_legal_person' => 'bool',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'processed_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
