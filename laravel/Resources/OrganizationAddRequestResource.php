<?php

namespace App\Http\Resources\Organization;

use App\Http\Resources\Resource;
use App\Http\Resources\UserListResource;

/**
 * @mixin \App\Models\OrganizationsAddRequest
 */
class OrganizationAddRequestResource extends Resource
{
    /**
     * @return array
     */
    protected function withRelations()
    {
        return ['author', 'author.profile.avatar'];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     * @throws \App\Exceptions\Pagination\PaginationException
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'isLegalPerson' => $this->is_legal_person,
            'feedbackUsername' => $this->feedback_username,
            'phone' => $this->phone,
            'email' => $this->email,
            'author' => new UserListResource($this->author),
        ];
    }
}
