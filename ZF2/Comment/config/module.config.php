<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Comment\Controller\Product' => 'Comment\Controller\ProductController',
            'Comment\Controller\Collection' => 'Comment\Controller\CollectionController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'comment' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'priority' => 1000,
                'options' => array(
                    'route' => '/comment/:controller',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Comment\Controller',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'change-rating' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => '/change-rating/:comment_id',
                            'defaults' => array(
                                'action'     => 'changeRating',
                            ),
                        ),
                    ),
                    'delete' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => '/delete',
                            'defaults' => array(
                                'action'     => 'deleteComment',
                            ),
                        ),
                    ),
                    'save' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => '/save',
                            'defaults' => array(
                                'action'     => 'saveComment',
                            ),
                        ),
                    ),
                    'list' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => '/list',
                            'defaults' => array(
                                'action'     => 'commentList',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => include __DIR__ .'/../template_map.php',
    ),
);
