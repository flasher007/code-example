<?php

namespace App\Http\Requests\Organization;

use App\Rules\Phone;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationAddRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'isLegalPerson' => 'required|boolean',
            'feedbackUsername' => 'required|string|max:60',
            'phone' => ['required', Phone::REGEX],
            'email' => 'required|email|max:255',
        ];
    }
}
