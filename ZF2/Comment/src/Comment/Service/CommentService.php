<?php

namespace Comment\Service;

use Basic\Service\BasicService;
use Basic\Service\Service;
use Comment\Model\DomainModel\CommentVote;
use Comment\Model\DomainModel\ProductCommentVote;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Blog\Model\BlogTable;
use Blog\Model\Select\BlogSelect;
use Basic\Service\CrudService;
use \Basic\Grid\Column;

/**
 * Базовый сервис комментариев
 * Class CommentService
 * @package Comment\Service
 */
abstract class CommentService extends BasicService
{
    /**
     * Объект таблицы работающий с комментариями
     *
     * @var \Comment\Model\Table\CommentTable
     */
    protected $commentTable = 'Comment\Model\Table\CommentTable';

    /**
     * Объект таблицы работающий с оценками комментариев
     *
     * @var \Comment\Model\Table\CommentVoteTable
     */
    protected $commentVoteTable = 'Comment\Model\Table\CommentVoteTable';

    /**
     * Объект таблицы работающий с оценками
     *
     * @var \Comment\Model\Table\VoteTable
     */
    protected $voteTable = 'Comment\Model\Table\VoteTable';

    protected $paginatorAdapterName = '\Basic\Db\RowsetPaginatorAdapter';

    /**
     * Возвращает имя класса адаптера для пагинации
     * @return string
     */
    public function getPaginatorAdapterName()
    {
        return $this->paginatorAdapterName;
    }

    /**
     * Возвращает имя класса адаптера для пагинации
     * @return string
     */
    public function getFormName()
    {
        return $this->formName;
    }

    /**
     * Устанавливает имя адаптера пагинатора для списка продуктов.
     * @param string $paginatorAdapterName
     * @return $this
     */
    public function setPaginatorAdapterName($paginatorAdapterName)
    {
        $this->paginatorAdapterName = $paginatorAdapterName;
        return $this;
    }

    /**
     * Возвращает объект таблицы для работы с комментариями
     * @return \Comment\Model\Table\CommentTable
     */
    public function getCommentTable()
    {
        if (is_string($this->commentTable)) {
            $this->commentTable = new $this->commentTable();
        }
        $this->commentTable->clearSelect();

        return $this->commentTable;
    }

    /**
     * Возвращает объект таблицы для работы с полезностью комментариев
     * @return \Comment\Model\Table\CommentVoteTable
     */
    public function getCommentVoteTable()
    {
        if (is_string($this->commentVoteTable)) {
            $this->commentVoteTable = new $this->commentVoteTable();
        }

        $this->commentVoteTable->clearSelect();

        return $this->commentVoteTable;
    }

    /**
     * Возвращает объект таблицы для работы с голосами за сущность
     * @return \Comment\Model\Table\VoteTable
     */
    public function getVoteTable()
    {
        if (is_string($this->voteTable)) {
            $this->voteTable = new $this->voteTable();
        }
        $this->voteTable->clearSelect();

        return $this->voteTable;
    }

    /**
     * Возвращает форму для добавления комментариев к заданной сущности от заданного пользователя
     * @param int|string $entityId - сущность к которой будут добавляться комментарии
     * @param \ZfcUser\Entity\User $user - пользователь для которого создается форма
     * @return \Basic\Form\Form
     */
    abstract public function getCommentForm($entityId, \ZfcUser\Entity\User $user = null);


    /**
     * @param \Comment\Model\Request\AddVoteRequest $addVoteRequest
     * @param \Comment\Form\ProductCommentForm      $form - форма из которой пришли эти данные
     * @param \ZfcUser\Entity\User               $user
     * @return bool
     */
    abstract public function saveDataOfCommentForm($addVoteRequest, $form, \ZfcUser\Entity\User $user = null);


    /**
     * Сохраняет текст имеющегося в базе комментария
     * @param string $commentText
     * @param int $commentId
     * @return bool
     */
    public function saveComment($commentText, $commentId)
    {
        $commentTable = $this->getCommentTable();
        try {
            $comment = $this->getCommentModel();
            $comment->setId($commentId);
            $comment->setComment($commentText);
            $success = (bool)$commentTable->save($comment);
            return $success;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Получить оценки товара
     * @param \Comment\Model\Request\VoteListRequest $request - запрос на выборку списка
     * @param null|int                            $commentsCount - количество комментов продукта
     * @return \Comment\Model\DomainModel\Interfaces\ProductVote\ProductComment[]|\Zend\Paginator\Paginator
     */
    public function getVotesByListRequest(\Comment\Model\Request\VoteListRequest $request, $commentsCount = null)
    {
        $voteTable = $this->getVoteTable();
        $votes = $voteTable->fetchAllWithCommentByRequest($request);
        $voteSelect = $voteTable->getSelect();

        $adapter = $this->getPaginatorAdapterName();
        $paginatorAdapter = new $adapter(
            $voteSelect,
            $votes
        );
        /* @var $paginatorAdapter \Basic\Db\RowsetPaginatorAdapter */
        if ($commentsCount !== null) {
            $paginatorAdapter->setRowCount($commentsCount);
        }

        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($request->page);
        $paginator->setItemCountPerPage($request->countPerPage);

        return $paginator;
    }

    /**
     * Возвращает отзывы пользователя к заданным коллекциям
     * @param int $userId - айдиха пользователя
     * @param \Comment\Model\DomainModel\Interfaces\ProductVote\Basic[] $productVotes - отзывы для которых найти отзывы пользователя о полезности
     * @return \Comment\Model\DomainModel\Interfaces\ProductCommentVote\Basic[]|\Basic\Db\ResultSet
     */
    public function getUserVotesToProductVotes($userId, $productVotes)
    {
        $ids = array();
        foreach ($productVotes as $vote) {
            if ($vote->getCommentId() != null) {
                $ids[] = $vote->getCommentId();
            }
        }
        $table = $this->getCommentVoteTable();;
        $select = $table->getSelect(true);

        return $table->fetchUserVotesOfComments($userId, $ids);
    }

    /**
     * Возвращает один голос
     * @param int $id - айдиха голоса
     * @return \Comment\Model\DomainModel\Interfaces\ProductComment\Basic|null
     */
    public function getVoteById($id)
    {
        $voteTable = $this->getVoteTable();
        return $voteTable->find($id);
    }

    /**
     * Удаляет один голос
     * @param int $id - айдиха голоса
     * @param int $ownerId
     * @return \Comment\Model\DomainModel\Interfaces\ProductComment\Basic|null
     */
    public function deleteVote($id, $ownerId)
    {
        $voteTable = $this->getVoteTable();
        $voteCommentTable = $this->getCommentTable();
        $voteTable->getAdapter()->getDriver()->getConnection()->beginTransaction();
        try {
            $res = $voteTable->deleteToTrash($id, $ownerId);
            $res = $res & $voteCommentTable->deleteToTrash($id, $ownerId);
            if ($res) {
                $voteTable->getAdapter()->getDriver()->getConnection()->commit();
            } else {
                $voteTable->getAdapter()->getDriver()->getConnection()->rollback();
                return false;
            }
        } catch (\Exception $e) {
            $voteTable->getAdapter()->getDriver()->getConnection()->rollback();
            return false;
        }

        return true;
    }

    /**
     * Добавляет голос к комментарию от заданного пользователя. Если голос уже был создан но с другим знаком позитивности,
     * то меняется только значение позитивности. Если же уже есть точно такой голос то ничего не делается. Если нет - то
     * добавляется
     *
     * @param int $userId - айдиха пользователя
     * @param int $commentId - айдиха коллекции
     * @param boolean $isPositive - добавлен ли плюс
     * @return \Comment\Model\DomainModel\Interfaces\ProductCommentVote\Basic|null
     */
    public function addVoteToComment($userId, $commentId, $isPositive)
    {
        $table = $this->getCommentVoteTable();
        $table->clearSelect();
        $votes = $table->fetchUserVotesOfComments($userId, $commentId);
        $vote = null;
        if (count($votes) > 0) {
            /**
             * @var CommentVote $vote
             */
            $vote = $votes->current();
            if ($vote->getIsPositive() != $isPositive) {
                $vote->setIsPositive($isPositive);
                $table->save($vote);
            } else {
                return null;
            }
        } else {
            $vote = $this->getCommentVoteModel();
            $vote->setUserId($userId)
                ->setCommentId($commentId)
                ->setIsPositive($isPositive);
            $table->save($vote);
        }

        return $vote;
    }

    /**
     * Возвращает пустую модель отзыва на комментарий
     * @return \Comment\Model\DomainModel\ProductCommentVote
     */
    abstract function getCommentVoteModel();

    /**
     * Возвращает пустую модель комментария
     * @return \Comment\Model\DomainModel\ProductComment
     */
    abstract function getCommentModel();

    /**
     * Возвращает пустую модель комментария
     * @return \Comment\Model\DomainModel\ProductVote
     */
    abstract function getVoteModel();

    /**
     * Выбрать комментарий по его айдихе
     * @param int $id
     * @return \Comment\Model\DomainModel\Interfaces\ProductComment\Basic|null
     */
    public function getComment($id)
    {
        $table = $this->getCommentTable();
        return $table->find($id);
    }
}