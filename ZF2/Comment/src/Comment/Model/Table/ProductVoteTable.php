<?php
namespace Comment\Model\Table;

use Comment\Model\Select\ProductCommentSelect;
use User\Model\Select\UserSelect;

/**
 * @method \Comment\Model\Select\ProductVoteSelect getSelect()
 */
class ProductVoteTable extends VoteTable
{
    protected $table = 'product_vote';
    protected $resultObjectPrototype = '\Comment\Model\DomainModel\ProductVote';
    protected $selectClassName = '\Comment\Model\Select\ProductVoteSelect';

    /**
     * @return \Comment\Model\Select\CommentSelect
     */
    public function getCommentSelect()
    {
        return new ProductCommentSelect();
    }
}